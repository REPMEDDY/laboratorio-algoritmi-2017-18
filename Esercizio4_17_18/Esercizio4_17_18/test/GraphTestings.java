
import esercizio4_17_18.Graph;
import esercizio4_17_18.GraphComparator;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author soryt
 */
public class GraphTestings<V> {

    Graph graph;

    @Before
    public void setUp() {
        this.graph = new Graph(new GraphComparator());
    }

    //start test
    @Test
    public void testOnCreateVertex() {
        int indexVertexAlba = this.graph.createVertex("Alba");
        int indexVertexTurin = this.graph.createVertex("Turin");
        assertTrue(indexVertexAlba < indexVertexTurin);
    }
    //end test

    //start test
    @Test
    public void testOnAdd() {
        int indexTurin = this.graph.createVertex("Turin");
        this.graph.add(indexTurin);
        assertTrue(1 == this.graph.getNumberOfKeyVertices());
    }
    //end test

    //start test
    @Test
    public void testOnCreateEdge() {
        int indexEdge1 = this.graph.createEdge(10);
        int indexEdge2 = this.graph.createEdge(4);
        assertTrue(indexEdge1 < indexEdge2);
    }
    //end test

    //start test
    @Test
    public void testOnSetDestination() {
        int indexVertex = this.graph.createVertex("Turin");
        int indexEdge = this.graph.createEdge(10);
        this.graph.setDestination(indexVertex, indexEdge);
        assertTrue("Turin" == this.graph.getDestination(indexEdge));
    }
    //end test

    //start test
    @Test
    public void testOnAddToAdjacencyList() {
        int keyVertexIndex = this.graph.createVertex("Alba");
        int vertex2 = this.graph.createVertex("Turin");
        int vertex3 = this.graph.createVertex("Milan");
        int vertex4 = this.graph.createVertex("Genoa");
        this.graph.add(keyVertexIndex);
        int edge2 = this.graph.createEdge(10);
        int edge3 = this.graph.createEdge(20);
        int edge4 = this.graph.createEdge(30);
        this.graph.setDestination(vertex2, edge2);
        this.graph.setDestination(vertex3, edge3);
        this.graph.setDestination(vertex4, edge4);
        this.graph.addToAdjacencyList(keyVertexIndex, edge2);
        this.graph.addToAdjacencyList(keyVertexIndex, edge3);
        this.graph.addToAdjacencyList(keyVertexIndex, edge4);
        List<Pair<V, Double>> valuesAndWeightsAdded = new ArrayList();
        valuesAndWeightsAdded.add(new Pair("Turin", 10.0));
        valuesAndWeightsAdded.add(new Pair("Milan", 20.0));
        valuesAndWeightsAdded.add(new Pair("Genoa", 30.0));
        List<Pair<V, Double>> adjacencyValues = this.graph.getAdjacencyListValues(keyVertexIndex);
        assertTrue(true == adjacencyValues.equals(valuesAndWeightsAdded) && (3 == this.graph.getAdjacencyListValues(keyVertexIndex).size()));
    }
    //end test

    //start test
    @Test
    public void testOnGetWeightOfGraph() {
        this.graph.createEdge(80);
        this.graph.createEdge(50);
        this.graph.createEdge(10);
        this.graph.createEdge(20);
        this.graph.createEdge(30);
        assertTrue((this.graph.getListEdgeLength() == 5) == true);
    }
    //end test

    //start test
    @Test
    public void testOnGetNumberOfVertices() {
        this.graph.createVertex("Alba");
        this.graph.createVertex("Turin");
        this.graph.createVertex("London");
        this.graph.createVertex("Venice");
        this.graph.add(this.graph.createVertex("Dublin"));
        assertTrue(true == (this.graph.getNumberOfVertices() == 1));
    }
    //end test

    //start test
    @Test
    public void testOnGetNumberOfKeyVertices() {
        this.graph.createVertex("Alba");
        this.graph.createVertex("Turin");
        this.graph.createVertex("London");
        this.graph.createVertex("Venice");
        this.graph.add(this.graph.createVertex("Dublin"));
        assertTrue(true == (this.graph.getNumberOfKeyVertices() == 1));
    }
    //end test

    //start test
    @Test
    public void testOnGetNumberOfEdges() {
        this.graph.createEdge(80);
        this.graph.createEdge(50);
        this.graph.createEdge(10);
        this.graph.createEdge(20);
        this.graph.createEdge(30);
        assertTrue((this.graph.getListEdgeLength() == 5) == true);
    }
    //end test
}
