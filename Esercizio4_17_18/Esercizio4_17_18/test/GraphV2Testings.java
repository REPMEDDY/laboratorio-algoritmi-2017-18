import esercizio4_17_18.GraphComparator;
import esercizio4_17_18.GraphException;
import esercizio4_17_18.GraphV2;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author soryt
 */
public class GraphV2Testings {
    
    private GraphV2 graph;
    
    @Before
    public void setUp() {
        this.graph = new GraphV2(new GraphComparator(), GraphV2.DIRECT_GRAPH);
    }
    
    @Test
    public void testOnAdd() throws GraphException{
        this.graph.add("Turin");
        this.graph.add("Milan");
        assertTrue(2 == this.graph.getNumberOfVertices());
    }
    
    @Test
    public void testOnAddAdjacent() throws GraphException {
        this.graph.add("Turin");
        this.graph.add("Milan");
        this.graph.add("Rome");
        this.graph.addAdjacent("Turin", "Milan", 220);
        this.graph.addAdjacent("Turin", "Rome", 1220);
        assertTrue(2 == this.graph.getAdjListLength("Turin"));
    }
    
    @Test
    public void testOnIsAlReadyPresent() throws GraphException{
        this.graph.add("Turin");
        assertTrue(true == this.graph.isAlReadyPresent("Turin"));
        assertTrue(false == this.graph.isAlReadyPresent("Milan"));
    }
    
    @Test
    public void testOnGetTotalWeight() throws GraphException{
        this.graph.add("Turin");
        this.graph.add("Milan");
        this.graph.add("Rome");
        this.graph.addAdjacent("Turin", "Milan", 220);
        this.graph.addAdjacent("Turin", "Rome", 1220);
        assertTrue(1440 == this.graph.totalWeight());
    }
    
    @Test
    public void testOnGetNumberOfEdges() throws GraphException {
        this.graph.add("Turin");
        this.graph.add("Milan");
        this.graph.add("Rome");
        this.graph.addAdjacent("Turin", "Milan", 220);
        this.graph.addAdjacent("Turin", "Rome", 1220);
        assertTrue(2 == this.graph.getNumberOfEdges());
    }
  

}
