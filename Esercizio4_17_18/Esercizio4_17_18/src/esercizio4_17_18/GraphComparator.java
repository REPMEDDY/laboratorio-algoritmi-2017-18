/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercizio4_17_18;

import java.util.Comparator;

/**
 *
 * @author soryt
 */
public class GraphComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Double e1 = (Double) o1;
        Double e2 = (Double) o2;
        if (e1.compareTo(e2) < 0) {
            return -1;
        } else if (e1.compareTo(e2) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

}
