package esercizio4_17_18;

import esercizio3_17_18.PriorityQueue;
import esercizio3_17_18.PriorityQueueComparator;
import esercizio3_17_18.PriorityQueueException;
import esercizio3_17_18.QueueElem;
import java.lang.annotation.Native;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javafx.util.Pair;

/**
 *
 * @author soryt
 */
public class GraphV2<V extends Comparable> {

    @Native
    public static final int NON_DIRECT_GRAPH = 0;
    @Native
    public static final int DIRECT_GRAPH = 1;
    private final List<Vertex> listVertex;
    private final Comparator comparator;
    private double totalWeight;
    private int numberOfEdges;
    int graph_type;

    public GraphV2(Comparator comparator, int graph_type) {
        this.comparator = comparator;
        this.listVertex = new ArrayList();
        this.totalWeight = 0;
        this.numberOfEdges = 0;
        this.graph_type = graph_type;
    }

    /**
     *
     * @return the number of vertices in the graph
     */
    public int getNumberOfVertices() {
        return this.listVertex.size();
    }

    /**
     * Add a vertex to the graph
     *
     * @param keyVertexValue: the value of the vertex to add
     * @throws GraphException if the graph already contains a vertex with that
     * value
     */
    public void add(V keyVertexValue) throws GraphException {
        if (!isAlReadyPresent(keyVertexValue)) {
            Vertex vertex = new Vertex(keyVertexValue);
            this.listVertex.add(vertex);
        } else {
            throw new GraphException("The graph already contains a vertex with value " + keyVertexValue);
        }
    }

    /**
     *
     * @param keyVertexValue: the value of the key vertex
     * @return the adjacency list of the vertex
     * @throws GraphException if the vertex is not stored in the graph
     */
    public int getAdjListLength(V keyVertexValue) throws GraphException {
        Vertex keyVertex = this.getVertex(keyVertexValue);
        if (keyVertex != null) {
            return keyVertex.adjacencyList.size();
        } else {
            throw new GraphException("This vertex is not stored in the graph");
        }
    }

    /**
     * Links two vertices with an edge
     *
     * @param keyVertexValue: the value of the key vertex
     * @param adjacentVertexValue: the value of the adjacent vertex
     * @param edgeWeight: the weight of the edge
     * @throws GraphException
     */
    public void addAdjacent(V keyVertexValue, V adjacentVertexValue, double edgeWeight) throws GraphException {
        Vertex keyVertex = this.getVertex(keyVertexValue);
        Vertex adjVertex = this.getVertex(adjacentVertexValue);
        if (this.graph_type == GraphV2.DIRECT_GRAPH) {
            if (keyVertex != null && adjVertex != null) {
                if (!isAlreadyPresentInAdjList(keyVertex, adjacentVertexValue)) {
                    keyVertex.adjacencyList.add(new Pair(new Edge(edgeWeight), adjVertex/*new Vertex(adjacentVertexValue)*/));
                    this.numberOfEdges++;
                    this.totalWeight += edgeWeight;
                }
            } else {
                throw new GraphException("Trying to add a vertex to the adjacency list of a null vertex or"
                        + "the vertex " + adjacentVertexValue + " is not stored in the graph");
            }
        } else {
            if (keyVertex != null && adjVertex != null) {
                Edge edge = new Edge(edgeWeight);
                if (!isAlreadyPresentInAdjList(keyVertex, adjacentVertexValue)) {
                    keyVertex.adjacencyList.add(new Pair(edge, adjVertex/*new Vertex(adjacentVertexValue)*/));
                }
                if (!isAlreadyPresentInAdjList(adjVertex, keyVertexValue)) {
                    adjVertex.adjacencyList.add(new Pair(edge, keyVertex/*new Vertex(keyVertexValue)*/));
                }
                this.numberOfEdges++;
                this.totalWeight += edgeWeight;
            } else {
                throw new GraphException("Trying to add a vertex to the adjacency list of a null vertex or"
                        + "the vertex " + adjacentVertexValue + " is not stored in the graph");
            }
        }
    }

    /**
     * Check if the vertex is already stored in the graph
     *
     * @param vertexValue: the value of the vertex
     * @return true if the vertex is already present, false otherwise
     */
    public boolean isAlReadyPresent(V vertexValue) {
        return this.listVertex.stream().anyMatch((vertex) -> (vertex.value.toString().equals(((String) vertexValue))));
    }

    /**
     *
     * @return the total weight of the graph
     */
    public double totalWeight() {
        return this.totalWeight;
    }

    /**
     *
     * @return the number of edges of the graph
     */
    public int getNumberOfEdges() {
        return this.numberOfEdges;
    }

    /**
     * Print the graph
     */
    public void print() {
        for (Vertex vertex : this.listVertex) {
            System.out.print("keyVertex: " + vertex.value + "adjList: ");
            for (Pair p : vertex.adjacencyList) {
                System.out.print(" " + ((Edge) p.getKey()).weight + " " + ((Vertex) p.getValue()).value);
            }
            System.out.println("\n");
        }
    }

    /**
     *
     * @param vertexValue: the value of the vertex
     * @return the vertex if found,null otherwise
     */
    private Vertex getVertex(V vertexValue) {
        for (Vertex vertex : this.listVertex) {
            if (vertex.value.toString().equals(vertexValue)) {
                return vertex;
            }
        }
        return null;
    }

    /**
     *
     * @param vertex: the key vertex
     * @param adjacentVertexValue: the value of the adjacent vertex
     * @return true if the vertex with value adjacentVertexValue is already
     * present in the adjacent list of the vertex, false otherwise
     */
    private boolean isAlreadyPresentInAdjList(Vertex vertex, V adjacentVertexValue) {
        return vertex.adjacencyList.stream().anyMatch((p) -> (((Vertex) p.getValue()).value.toString().equals(((String) adjacentVertexValue))));
    }

    private PriorityQueue initialize() throws PriorityQueueException {
        PriorityQueue priorityQueue = new PriorityQueue(new PriorityQueueComparator(), PriorityQueue.MIN_PRIORITY_QUEUE);
        for (int i = 1; i < this.listVertex.size(); i++) {
            this.listVertex.get(i).minEdgeWeightThatLinksAVertexToAnotherVertex = Double.MAX_VALUE;
            this.listVertex.get(i).priorityQueueId = i;
            this.listVertex.get(i).listVertexIndex = i;
            QueueElem queueElem = new QueueElem(Double.MAX_VALUE, i);
            queueElem.setQueueElemId(i);
            priorityQueue.insert(queueElem);

        }
        this.listVertex.get(0).minEdgeWeightThatLinksAVertexToAnotherVertex = 0.0;
        this.listVertex.get(0).priorityQueueId = 0;
        this.listVertex.get(0).listVertexIndex = 0;
        QueueElem queueElem = new QueueElem(0.0, 0);
        queueElem.setQueueElemId(0);
        priorityQueue.insert(queueElem);

        return priorityQueue;
    }

    public GraphV2 mstPrimAlgorithm() throws PriorityQueueException, GraphException {
        PriorityQueue priorityQueue = this.initialize();
        GraphV2 mst = new GraphV2(new GraphComparator(), GraphV2.DIRECT_GRAPH);
        int vertexIndex;
        while (priorityQueue.getNumberOfElements() > 0) {
            QueueElem currMinElem = priorityQueue.extractMin();
            vertexIndex = (int) currMinElem.getValue();
            for (Pair adjVertex : this.listVertex.get(vertexIndex).adjacencyList) {
                double adjVertexWeight = ((Edge) adjVertex.getKey()).weight;
                if (priorityQueue.isStoredInQueue(((Vertex) adjVertex.getValue()).priorityQueueId)
                        && adjVertexWeight < ((Vertex) adjVertex.getValue()).minEdgeWeightThatLinksAVertexToAnotherVertex) {
                    /*if (!mst.isAlReadyPresent(this.listVertex.get(vertexIndex).value)) {
                        mst.add(this.listVertex.get(vertexIndex).value);                      
                    }
                    if (!mst.isAlReadyPresent(((Vertex) adjVertex.getValue()).value)) {
                        mst.add(((Vertex) adjVertex.getValue()).value);
                    }
                    mst.addAdjacent(this.listVertex.get(vertexIndex).value, ((Vertex) adjVertex.getValue()).value, adjVertexWeight);*/
                    ((Vertex) adjVertex.getValue()).minEdgeWeightThatLinksAVertexToAnotherVertex = adjVertexWeight;
                    this.listVertex.get(((Vertex) adjVertex.getValue()).listVertexIndex).minEdgeWeightThatLinksAVertexToAnotherVertex = adjVertexWeight;
                    this.listVertex.get(vertexIndex).adjVertexIndex = ((Vertex) adjVertex.getValue()).listVertexIndex;
                    QueueElem adjElem = priorityQueue.getQueueElemById(((Vertex) adjVertex.getValue()).priorityQueueId);
                    priorityQueue.decreasePriority(adjElem, adjElem.getQueueElemIndex(), adjVertexWeight);
                }
            }
        }
        /*int adjVertexToAddIndex = -1;
        double weight = Double.MAX_VALUE;
        for (Vertex keyVertex : this.listVertex) {
            for (Pair adjVertex : keyVertex.adjacencyList) {
                if (((Edge) adjVertex.getKey()).weight < weight) {
                    weight = ((Edge) adjVertex.getKey()).weight;       
                    adjVertexToAddIndex = ((Vertex) adjVertex.getValue()).listVertexIndex;
                }
            }
            if (!mst.isAlReadyPresent(keyVertex.value)) {
                mst.add(keyVertex.value);
            }
            if (!mst.isAlReadyPresent(this.listVertex.get(adjVertexToAddIndex).value)) {
                mst.add(this.listVertex.get(adjVertexToAddIndex).value);
            }
            mst.addAdjacent(keyVertex.value, this.listVertex.get(adjVertexToAddIndex).value, weight);
        }*/
        double key = Double.MAX_VALUE;
        int adjVertexToAddIndex = -1;
        for (Vertex keyVertex : this.listVertex) {
            keyVertex.discovered = true;
            //System.out.println("Vertice chiave : " + keyVertex.value);
            for (Pair adjVertex : keyVertex.adjacencyList) {
                if (((Vertex) adjVertex.getValue()).discovered == false && ((Vertex) adjVertex.getValue()).minEdgeWeightThatLinksAVertexToAnotherVertex < key) {
                    key = ((Vertex) adjVertex.getValue()).minEdgeWeightThatLinksAVertexToAnotherVertex;
                    adjVertexToAddIndex = ((Vertex) adjVertex.getValue()).listVertexIndex;
                }
            }
            // System.out.println("Peso dell'arco da aggiungere: " + key);
            if (!mst.isAlReadyPresent(keyVertex.value)) {
                mst.add(keyVertex.value);
            }
            if (!mst.isAlReadyPresent(this.listVertex.get(adjVertexToAddIndex).value)) {
                mst.add(this.listVertex.get(adjVertexToAddIndex).value);
            }
            mst.addAdjacent(keyVertex.value, this.listVertex.get(adjVertexToAddIndex).value, key);

        }
        return mst;
    }

    private class Vertex {

        private final V value;
        private double minEdgeWeightThatLinksAVertexToAnotherVertex;
        private final List<Pair<Edge, Vertex>> adjacencyList;
        private int listVertexIndex;
        private int adjVertexIndex;
        private double priorityQueueId;
        private boolean discovered = false;

        public Vertex(V value) {
            this.value = value;
            this.adjacencyList = new ArrayList();
        }

    }//end class Vertex

    private class Edge {

        private final double weight;

        public Edge(double weight) {
            this.weight = weight;
        }

    }//end class Edge
}
