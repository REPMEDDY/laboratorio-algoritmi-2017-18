package esercizio4_17_18;

import esercizio3_17_18.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;

/**
 * This class implements the graph data structure using adjacency lists. This
 * implementation supports both direct and indirect graphs. The adjacency list
 * implementation is made with a Map object where keys are the vertices while
 * the values are the adjacency lists.
 *
 * @author soryt
 */
public class Graph<V> {

    private final List<Vertex> listVertex;
    private final List<Edge> listEdge;
    private final Map<V, List<Edge>> graph;
    private final Comparator comparator;
    private double weight;
    private int vertices;
    private int edges;

    public Graph(Comparator comparator) {
        this.graph = new HashMap();
        this.comparator = comparator;
        this.listEdge = new ArrayList();
        this.listVertex = new ArrayList();
        this.weight = 0;
        this.vertices = 0;
        this.edges = 0;
    }
    
    public int getListVertexLength() {
        return this.listVertex.size();
    }
    
    public int getListEdgeLength() {
        return this.listEdge.size();
    }

    /**
     * Prints all the key Vertices in the graph. A key vertex is a vertex that
     * has at least one vertex in its adjacency list
     */
    public void printKeyVertices() {
        this.graph.keySet().forEach((item) -> {
            System.out.println(item);
        });
    }

    /**
     * Check if the vertex is a key vertex
     *
     * @param value: the value of the vertex
     * @return true if the vertex with the value passed as parameter is stored
     * in the graph, false otherwise
     */
    public boolean isKeyVertex(V value) {
        return this.graph.keySet().contains(value);
    }

    /**
     * Check if the vertex with the value adjVertexValue is part of the
     * adjacency list of the key vertex with value keyVertexValue
     *
     * @param keyVertexValue: the value of the key vertex
     * @param adjVertexValue: the value of the adjacent vertex
     * @return true if the vertex is part of the adjacency list of the key
     * vertex
     */
    public boolean isAdjValuePresent(V keyVertexValue, V adjVertexValue) {
        return this.graph.get(keyVertexValue).stream().anyMatch((edge) -> (edge.destination.value.toString().equals(((String) adjVertexValue))));
    }

    /**
     * Create a new vertex for the graph assigning a value to it.
     *
     * @param value: the value to assign to the vertex
     * @return an index to refer to the vertex in the graph
     */
    public int createVertex(V value) {
        Vertex vertex = new Vertex(value);
        this.listVertex.add(vertex);
        this.listVertex.get(this.listVertex.size() - 1).listVertexIndex = this.listVertex.size() - 1;
        return this.listVertex.size() - 1;
    }

    /**
     * Add a vertex to the graph as a key
     *
     * @param vertexIndex: an index to refer to the vertex to add
     * @return an index to refer to the key vertex in the graph
     */
    public void add(int vertexIndex) {
        this.graph.put(this.listVertex.get(vertexIndex).value, null);
        this.vertices++;
    }

    /**
     * Create a new edge for the graph and assign a weight to it
     *
     * @param weight: the weight to assign to the edge
     * @return an index to refer to the edge in the graph
     */
    public int createEdge(double weight) {
        Edge edge = new Edge(weight);
        this.listEdge.add(edge);
        return this.listEdge.size() - 1;
    }

    /**
     * Set a destination vertex for an edge. A vertex is a destination vertex if
     * an edge incises on a vertex.
     *
     * @param vertexIndex: an index to refer to the destination vertex in the
     * graph
     * @param edgeIndex: an index to refer to an edge in the graph
     */
    public void setDestination(int vertexIndex, int edgeIndex) {
        this.listEdge.get(edgeIndex).destination = this.listVertex.get(vertexIndex);
    }
    
    /**
     * Set a destination vertex for an edge. A vertex is a destination vertex if
     * an edge incises on a vertex.
     * 
     * @param vertexValue: the value of the vertex to set as destination vertex
     * @param edgeIndex: the index of the edge 
     */
    public void setDestination(V vertexValue, int edgeIndex) {
        this.listEdge.get(edgeIndex).destination = new Vertex(vertexValue);
    }

    /**
     * Add a vertex to the adjacency list of a key vertex in the graph
     *
     * @param keyVertexIndex: an index to refer to the key vertex
     * @param edgeIndex: an index to refer to the edge that stores the
     * destination vertex to add to the adjacency list of the key vertex
     */
    public void addToAdjacencyList(int keyVertexIndex, int edgeIndex) {
        if (this.graph.get(this.listVertex.get(keyVertexIndex).value) != null) {
            this.graph.get(this.listVertex.get(keyVertexIndex).value).add(this.listEdge.get(edgeIndex));
        } else {
            List<Edge> adjacencyList = new ArrayList();
            adjacencyList.add(this.listEdge.get(edgeIndex));
            this.graph.put(this.listVertex.get(keyVertexIndex).value, adjacencyList);
        }
        this.weight += this.listEdge.get(edgeIndex).weight;
    }
    
    /**
     * Add a vertex to the adjacency list of a key vertex in the graph
     * 
     * @param keyVertexValue: the value of the key vertex
     * @param edgeIndex: an index to refer to the edge that stores the
     * destination vertex to add to the adjacency list of the key vertex 
     */
    public void addToAdjacencyList(V keyVertexValue, int edgeIndex) {
        this.graph.get(keyVertexValue).add(this.listEdge.get(edgeIndex));
        this.weight += this.listEdge.get(edgeIndex).weight;
    }

    /**
     *
     * @param edgeIndex
     * @return the value of the destination vertex associated to the edge at
     * edgeIndex position
     */
    public V getDestination(int edgeIndex) {
        return (V) this.listEdge.get(edgeIndex).destination.value;
    }

    /**
     *
     * @return the total weight of the graph
     */
    public double getWeightOfGraph() {
        return this.weight;
    }

    /**
     *
     * @return the number of vertices stored in the graph
     */
    public int getNumberOfVertices() {
        return this.vertices;
    }

    /**
     * A key vertex is a vertex with at least one vertex in its adjacency list
     *
     * @return the number of key vertices
     */
    public int getNumberOfKeyVertices() {
        return this.graph.keySet().size();
    }

    /**
     * An edge links a key vertex to another vertex
     *
     * @return the number of edges stored in the graph
     */
    public int getNumberOfEdges() {
        for (V keyValue : this.graph.keySet()) {
            this.edges += this.graph.get(keyValue).size() / 2;
        }
        return this.edges;
    }

    /**
     * The adjacency list of a key vertex is a list of all the vertices and
     * edges that are linked to the key vertex with one edge
     *
     * @param keyVertexIndex: the index of the key vertex
     * @return a list of the values and the weights of the adjacency list of the
     * key vertex or null if there's no adjacency list for the given key vertex
     */
    public List<Pair<V, Double>> getAdjacencyListValues(int keyVertexIndex) {
        List<Pair<V, Double>> adjacencyList = new ArrayList();
        this.graph.get(this.listVertex.get(keyVertexIndex).value).forEach((edge) -> {
            adjacencyList.add(new Pair((V) edge.destination.value, edge.weight));
        });
        return adjacencyList;
    }

    /**
     * The adjacency list of a key vertex is a list of all the vertices and
     * edges that are linked to the key vertex with one edge
     * 
     * @param keyVertexValue: the value of the key vertex
     * @return a list of the values and the weights of the adjacency list of the
     * key vertex or null if there's no adjacency list for the given key vertex
     */
    public List<Pair<V, Double>> getAdjacencyListValues(V keyVertexValue) {
        List<Pair<V, Double>> adjacencyList = new ArrayList();
        this.graph.get(keyVertexValue).forEach((edge) -> {
            adjacencyList.add(new Pair((V) edge.destination.value, edge.weight));
        });
        return adjacencyList;
    }

    /**
     * The adjacency list of a key vertex is a list of all the vertices that are
     * linked to the key vertex with one edge
     *
     * @param keyVertexIndex: the index of the key vertex
     * @return the adjacency list of the key vertex or null if there's no
     * adjacency list for the given key vertex
     */
    private List<Edge> getAdjacencyList(int keyVertexIndex) {
        return this.graph.get(this.listVertex.get(keyVertexIndex).value);
    }

    /**
     * Initialize the min priority queue for the Prim algorithm
     *
     * @return the initialized min priority queue
     * @throws PriorityQueueException
     */
    private PriorityQueue initializePriorityQueue() throws PriorityQueueException {
        PriorityQueue priorityQueue = new PriorityQueue(this.comparator, PriorityQueue.MIN_PRIORITY_QUEUE);

        for (int i = 1; i < this.listVertex.size(); i++) {
            /*QueueElem queueElem = new QueueElem(
                    this.listVertex.get(i).minEdgeWeightThatLinksAVertexToAnotherVertex = Double.MAX_VALUE,
                    new Pair(i, this.listVertex.get(i)));*/
            QueueElem queueElem = new QueueElem(
                    this.listVertex.get(i).minEdgeWeightThatLinksAVertexToAnotherVertex = Double.MAX_VALUE, i);
            priorityQueue.insert(queueElem);
        }
        /*priorityQueue.insert(new QueueElem(
                this.listVertex.get(0).minEdgeWeightThatLinksAVertexToAnotherVertex = 0,
                new Pair(0, this.listVertex.get(0))));*/
        priorityQueue.insert(new QueueElem(
                this.listVertex.get(0).minEdgeWeightThatLinksAVertexToAnotherVertex = 0, 0));
        System.out.println(priorityQueue.getNumberOfElements());
        return priorityQueue;
    }

    /**
     * Build a minimum spanning tree for the graph. A minimum spanning tree is a
     * graph that links all the vertices in the graph choosing the edges with
     * the minimum weight.
     *
     * @return the minimum spanning tree
     */
    public Graph minimumSpanningTreePrimAlgorithm() throws PriorityQueueException {
        PriorityQueue priorityQueue = this.initializePriorityQueue();
        Graph minimumSpanningTree = new Graph(new GraphComparator());

        while (priorityQueue.getNumberOfElements() > 0) {
            QueueElem currMinElem = priorityQueue.extractMin();
            //System.out.println("priorita di currMinElem: " + currMinElem.getPriority());
            //List<Edge> adjacencyList = this.getAdjacencyList((Integer) ((Pair) currMinElem.getValue()).getKey());
            List<Edge> adjacencyList = this.getAdjacencyList((Integer) currMinElem.getValue());
            if (adjacencyList.size() > 0) {
                for (Edge edge : adjacencyList) {
                    /*if ((priorityQueue.search(
                            edge.destination.minEdgeWeightThatLinksAVertexToAnotherVertex)).size() > 0
                           ((QueueElem adjElem = priorityQueue.getQueueElem(edge.destination.listVertexIndex)) != null && (this.comparator.compare(edge.weight, edge.destination.minEdgeWeightThatLinksAVertexToAnotherVertex) < 0)) {*/
                    QueueElem adjElem = priorityQueue.getQueueElem(edge.destination.listVertexIndex);
                    //System.out.println("adjElem: " + adjElem);
                    if (adjElem != null && (this.comparator.compare(edge.weight, /*adjElem.getPriority()*/ edge.destination.minEdgeWeightThatLinksAVertexToAnotherVertex) < 0)) {
                        edge.destination.minEdgeWeightThatLinksAVertexToAnotherVertex = edge.weight;
                        priorityQueue.decreasePriority(adjElem, adjElem.getQueueElemIndex(), edge.weight);
                        int vertexIndexDestination;
                        int vertexIndexMST = 0;
                        if (!minimumSpanningTree.isKeyVertex(this.listVertex.get((Integer) currMinElem.getValue()).value)) {
                            vertexIndexMST = minimumSpanningTree.createVertex(this.listVertex.get((Integer) currMinElem.getValue()).value);
                            minimumSpanningTree.add(vertexIndexMST);
                            int edgeIndexMST = minimumSpanningTree.createEdge(edge.weight);
                            vertexIndexDestination = minimumSpanningTree.createVertex(edge.destination.value);
                            minimumSpanningTree.setDestination(vertexIndexDestination, edgeIndexMST);
                            minimumSpanningTree.addToAdjacencyList(vertexIndexMST, edgeIndexMST);
                        } else {
                            int edgeIndexMST = minimumSpanningTree.createEdge(edge.weight);
                            vertexIndexDestination = minimumSpanningTree.createVertex(edge.destination.value);
                            minimumSpanningTree.setDestination(vertexIndexDestination, edgeIndexMST);
                            minimumSpanningTree.addToAdjacencyList(this.listVertex.get((Integer) currMinElem.getValue()).value, edgeIndexMST);
                        }
                        if (!minimumSpanningTree.isKeyVertex(edge.destination.value)) {
                            minimumSpanningTree.add(vertexIndexDestination);
                            int edgeIndexReverse = minimumSpanningTree.createEdge(edge.weight);
                            minimumSpanningTree.setDestination(vertexIndexMST, edgeIndexReverse);
                            minimumSpanningTree.addToAdjacencyList(vertexIndexDestination, edgeIndexReverse);
                        } else {
                            int edgeIndexReverse = minimumSpanningTree.createEdge(edge.weight);
                            minimumSpanningTree.setDestination(vertexIndexMST, edgeIndexReverse);
                            minimumSpanningTree.addToAdjacencyList(vertexIndexDestination, edgeIndexReverse);
                        }
                    }
                    // }//end vecchio if
                }
            }
        }
        return minimumSpanningTree;
    }
    
    /**
     * Print the graph
     */
    public void print() {
        String adj = "";
        String vertex;
        for (V value : this.graph.keySet()) {
            vertex = value.toString() + " --";
            for (Edge edge : this.graph.get(value)) {
                adj = adj + edge.weight + "--> " + edge.destination.value + " ";
            }
            System.out.println(vertex + adj);
            adj = "";
        }
    }

    /**
     * This inner class represent the edges of the graph. An edge links together
     * two vertexes. In this implementation the edges can have or not have a
     * weight.
     *
     */
    private class Edge {

        private Vertex destination;
        private final double weight;

        private Edge(double weight) {
            this.weight = weight;
        }
    }//end Edge class

    /**
     * This inner class represent the vertexes of the graph. A vertex contains a
     * value.
     *
     * @param <V>: the type of the value of the vertex
     */
    private class Vertex {

        private final V value;
        private double minEdgeWeightThatLinksAVertexToAnotherVertex;
        private int listVertexIndex;

        private Vertex(V value) {
            this.value = value;
        }
    }//end Vertex class
}
