package UseCasePrimAlgorithm;

import esercizio3_17_18.PriorityQueueException;
import esercizio4_17_18.Graph;
import esercizio4_17_18.GraphComparator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class UseCasePrimAlgorithm {

    static FileReader fr;
    static BufferedReader br;
    static Graph graph = new Graph(new GraphComparator());

    /**
     * Create A ---> B and B ---> A
     *
     * @param currLineFields
     */
    static int buildGraphField0NotPresent(String[] currLineFields) {
        int indexField0 = graph.createVertex(currLineFields[0]);
        graph.add(indexField0);
        int indexEdge = graph.createEdge(Double.parseDouble(currLineFields[2]));
        int indexField1 = graph.createVertex(currLineFields[1]);
        graph.setDestination(indexField1, indexEdge);
        graph.addToAdjacencyList(indexField0, indexEdge);

        if (!graph.isKeyVertex(currLineFields[1])) {
            graph.add(indexField1);
            int indexEdge2 = graph.createEdge(Double.parseDouble(currLineFields[2]));
            graph.setDestination(indexField0, indexEdge2);
            graph.addToAdjacencyList(indexField1, indexEdge2);
        } else {// currLineFields[1] is a key vertex
            int indexEdge2 = graph.createEdge(Double.parseDouble(currLineFields[2]));
            graph.setDestination(indexField0, indexEdge2);
            graph.addToAdjacencyList(indexField1, indexEdge2);
        }
        return indexField0;
    }

    static void buildGraphField0IsPresent(String[] currLineFields, int indexField0) {
        int indexField1 = graph.createVertex(currLineFields[1]);
        int indexEdge = graph.createEdge(Double.parseDouble(currLineFields[2]));
        graph.setDestination(indexField1, indexEdge);
        graph.addToAdjacencyList(indexField0, indexEdge);

        if (!graph.isKeyVertex(currLineFields[1])) {
            graph.add(indexField1);
            int indexEdge2 = graph.createEdge(Double.parseDouble(currLineFields[2]));
            graph.setDestination(indexField0, indexEdge2);
            graph.addToAdjacencyList(indexField1, indexEdge2);
        } else {//currLineFields[1] is a key vertex
            int indexEdge2 = graph.createEdge(Double.parseDouble(currLineFields[2]));
            graph.setDestination(indexField0, indexEdge2);
            graph.addToAdjacencyList(indexField1, indexEdge2);
        }
    }

    static void buildGraphField0IsPresent(String[] currLineFields) {
        int indexField1 = graph.createVertex(currLineFields[1]);
        int indexEdge = graph.createEdge(Double.parseDouble(currLineFields[2]));
        graph.setDestination(indexField1, indexEdge);
        graph.addToAdjacencyList(currLineFields[0], indexEdge);

        if (!graph.isKeyVertex(currLineFields[1])) {
            graph.add(indexField1);
            int indexEdge2 = graph.createEdge(Double.parseDouble(currLineFields[2]));
            graph.setDestination(currLineFields[0], indexEdge2);
            graph.addToAdjacencyList(indexField1, indexEdge2);
        } else {//currLineFields[1] is a key vertex
            int indexEdge2 = graph.createEdge(Double.parseDouble(currLineFields[2]));
            graph.setDestination(currLineFields[0], indexEdge2);
            graph.addToAdjacencyList(indexField1, indexEdge2);
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, PriorityQueueException {
        fr = new FileReader("italian_dist_graph.csv");
        br = new BufferedReader(fr);
        String currLine = "abadia a isola,staggia,3943.5861657554074";
        String field0;
        String[] currLineFields;
        int indexField0;
        br.readLine();//skip the first line in the csv file because it's already saved in currLine
        while (currLine != null) {
            currLineFields = currLine.split(",");
            field0 = currLineFields[0];
            if (!graph.isKeyVertex(field0)) {
                indexField0 = buildGraphField0NotPresent(currLineFields);
                while (((currLine = br.readLine()) != null) && (currLineFields = currLine.split(",")) != null && currLineFields[0].equals(field0)) {
                    buildGraphField0IsPresent(currLineFields, indexField0);
                }               
            } else {//field0 is a key vertex
                buildGraphField0IsPresent(currLineFields);
                while (((currLine = br.readLine()) != null) && (currLineFields = currLine.split(",")) != null && currLineFields[0].equals(field0)) {
                    buildGraphField0IsPresent(currLineFields);
                }
            }          
        }
        br.close();
        fr.close();
        System.out.println("GRAFO DI PARTENZA");
        //graph.print();
        System.out.println("Peso tot grafo partenza: " + graph.getWeightOfGraph());
        System.out.println("Numero nodi grafo partenza: " + graph.getNumberOfVertices());
        System.out.println("Numero archi grafo partenza: " + graph.getNumberOfEdges());
        Graph mst = graph.minimumSpanningTreePrimAlgorithm();
        System.out.println("\n");
        System.out.println("MST");
        //mst.print();
        System.out.println("Peso tot mst: " + mst.getWeightOfGraph());
        System.out.println("Numero nodi mst: " + mst.getNumberOfVertices());
        System.out.println("Numero archi mst: " + mst.getNumberOfEdges());

    }
}
