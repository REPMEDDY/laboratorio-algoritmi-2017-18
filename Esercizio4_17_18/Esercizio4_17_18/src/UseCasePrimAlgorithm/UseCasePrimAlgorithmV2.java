/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UseCasePrimAlgorithm;

import esercizio3_17_18.PriorityQueueException;
import esercizio4_17_18.GraphComparator;
import esercizio4_17_18.GraphException;
import esercizio4_17_18.GraphV2;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author soryt
 */
public class UseCasePrimAlgorithmV2 {

    public static void main(String[] args) throws FileNotFoundException, IOException, GraphException, PriorityQueueException {
        FileReader fr = new FileReader("italian_dist_graph.csv");
        BufferedReader br = new BufferedReader(fr);
        GraphV2 graph = new GraphV2(new GraphComparator(), GraphV2.NON_DIRECT_GRAPH);
        String[] currLineFields;
        String currLine;
        while ((currLine = br.readLine()) != null) {
            currLineFields = currLine.split(",");
            if (!graph.isAlReadyPresent(currLineFields[0])) {
                graph.add(currLineFields[0]);
            }
            if (!graph.isAlReadyPresent(currLineFields[1])) {
                graph.add(currLineFields[1]);
            }
            graph.addAdjacent(currLineFields[0], currLineFields[1], Double.parseDouble(currLineFields[2]));
            //System.out.println(graph.getAdjListLength(currLineFields[0]));
            //System.out.println(graph.getAdjListLength(currLineFields[1]));
        }
        System.out.println("Peso grafo: " + graph.totalWeight());
        System.out.println("Numero vertici grafo: " + graph.getNumberOfVertices());
        System.out.println("Numero archi grafo: " + graph.getNumberOfEdges());
        //graph.print();
        System.out.println("\n");
        GraphV2 mst = graph.mstPrimAlgorithm();
        System.out.println("Peso mst: " + mst.totalWeight());
        System.out.println("Numero vertici mst: " + mst.getNumberOfVertices());
        System.out.println("Numero archi mst: " + mst.getNumberOfEdges());
        //mst.print();
        br.close();
        fr.close();
    }
}
