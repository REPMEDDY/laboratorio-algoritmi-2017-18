import esercizio2_17_18.EditDistanceCalculator;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author soryt
 */
public class EditDistanceCalculatorTestings {
    
    private EditDistanceCalculator editDistanceCalculator1; //testEditDistanceDinOnNullStrings()
    private EditDistanceCalculator editDistanceCalculator2; //testEditDistanceDinOneStringWithZeroCharacters()
    private EditDistanceCalculator editDistanceCalculator3; //testEditDistanceDinTwoStrings()
    private EditDistanceCalculator editDistanceCalculator4; //testEditDistanceDinTwoEqualStrings()
    private EditDistanceCalculator editDistanceCalculator5; //testEditDistanceRecursiveOnNullStrings()
    private EditDistanceCalculator editDistanceCalculator6; //testEditDistanceRecursiveOneStringWithZeroCharacters()
    private EditDistanceCalculator editDistanceCalculator7; //testEditDistanceRecursiveTwoStrings()
    private EditDistanceCalculator editDistanceCalculator8; //testEditDistanceRecursiveTwoEqualStrings()
    
    //Start test
    @Before
    public void setUpTestEditDistanceDinOnNullStrings() {
        this.editDistanceCalculator1 = new EditDistanceCalculator();
        this.editDistanceCalculator1.setS1(null);
        this.editDistanceCalculator1.setS2(null);
    }
    
    @Test
    public void testEditDistanceDinOnNullStrings() {
        assertTrue(-1 == this.editDistanceCalculator1.editDistanceDin());
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestEditDistanceDinOneStringWithZeroCharacters(){
        this.editDistanceCalculator2 = new EditDistanceCalculator();
        this.editDistanceCalculator2.setS1("");
        this.editDistanceCalculator2.setS2("string");
    }
    
    @Test
    public void testEditDistanceDinOneStringWithZeroCharacters(){
        assertTrue(this.editDistanceCalculator2.getS2().length() == this.editDistanceCalculator2.editDistanceDin());
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestEditDistanceDinTwoStrings(){
        this.editDistanceCalculator3 = new EditDistanceCalculator();
        this.editDistanceCalculator3.setS1("tassa");
        this.editDistanceCalculator3.setS2("passato");
    }
    
    @Test
    public void testEditDistanceDinTwoStrings(){
        assertTrue(4 == this.editDistanceCalculator3.editDistanceDin());
    }
    //End test
    
    //Start test
    @Before
    public void setUpEditDistanceDinTwoEqualStrings(){
        this.editDistanceCalculator4 = new EditDistanceCalculator();
        this.editDistanceCalculator4.setS1("string");
        this.editDistanceCalculator4.setS2("string");
    }
    
    @Test
    public void testEditDistanceDinTwoEqualStrings(){
        assertTrue(0 == this.editDistanceCalculator4.editDistanceDin());
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestEditDistanceRecursiveOnNullStrings() {
        this.editDistanceCalculator5 = new EditDistanceCalculator();
        this.editDistanceCalculator5.setS1(null);
        this.editDistanceCalculator5.setS2(null);
    }
    
    @Test
    public void testEditDistanceRecursiveOnNullStrings() {
        assertTrue(-1 == this.editDistanceCalculator5.editDistanceRecursive(
                this.editDistanceCalculator5.getS1(), this.editDistanceCalculator5.getS2()));
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestEditDistanceRecursiveOneStringWithZeroCharacters(){
        this.editDistanceCalculator6 = new EditDistanceCalculator();
        this.editDistanceCalculator6.setS1("");
        this.editDistanceCalculator6.setS2("string");
    }
    
    @Test
    public void testEditDistanceRecursiveOneStringWithZeroCharacters(){
        assertTrue(this.editDistanceCalculator6.getS2().length() == this.editDistanceCalculator6.editDistanceRecursive(
                this.editDistanceCalculator6.getS1(), this.editDistanceCalculator6.getS2()));
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestEditDistanceRecursiveTwoStrings(){
        this.editDistanceCalculator7 = new EditDistanceCalculator();
        this.editDistanceCalculator7.setS1("tassa");
        this.editDistanceCalculator7.setS2("passato");
    }
    
    @Test
    public void testEditDistanceRecursiveTwoStrings(){
        assertTrue(4 == this.editDistanceCalculator7.editDistanceRecursive(
                this.editDistanceCalculator7.getS1(), this.editDistanceCalculator7.getS2()));    
    }
    //End test
    
    //Start test
    @Before
    public void setUpEditDistanceRecursiveTwoEqualStrings(){
        this.editDistanceCalculator8 = new EditDistanceCalculator();
        this.editDistanceCalculator8.setS1("string");
        this.editDistanceCalculator8.setS2("string");
    }
    
    @Test
    public void testEditDistanceRecursiveTwoEqualStrings(){
        assertTrue(0 == this.editDistanceCalculator8.editDistanceRecursive(
                this.editDistanceCalculator8.getS1(), this.editDistanceCalculator8.getS2()));
    }
    //End test
}
