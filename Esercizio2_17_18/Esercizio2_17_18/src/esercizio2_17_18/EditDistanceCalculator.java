package esercizio2_17_18;

/**
 * This class provide methods to compute the edit distance between two strings
 * s1 and s2 in order to transform s2 in s1
 *
 * @author soryt
 */
public class EditDistanceCalculator {

    private String s1;
    private String s2;

    public void setS1(String s1) {
        this.s1 = s1;
    }

    public void setS2(String s2) {
        this.s2 = s2;
    }

    public String getS1() {
        return this.s1;
    }

    public String getS2() {
        return this.s2;
    }

    /**
     * This method ignore the first character of the string
     * 
     * @param s: the string on which the first character is ignored
     * @return the string s without the first character
     */
    private String rest(String s) {
        return s.substring(1);
    }
    
    /**
     * This method compute the edit distance between two strings s1 and s2
     * recursively
     *
     * @param s1
     * @param s2
     * @return the edit distance between s1 and s2 or -1 if s1 and s2 are both
     * null
     */
    public int editDistanceRecursive(String s1, String s2) {
        if (s1 != null && s2 != null) {
            if (s1.length() == 0 && s2.length() > 0) {
                return s2.length();
            } else if (s1.length() > 0 && s2.length() == 0) {
                return s1.length();
            } else if (s1.equals(s2)) {
                return 0;
            } else {
                int distanceNoOp = Integer.MAX_VALUE, distanceCanc, distanceInsert;
                if (s1.charAt(0) == s2.charAt(0)) {
                    distanceNoOp = this.editDistanceRecursive(this.rest(s1), this.rest(s2));
                }
                distanceCanc = 1 + this.editDistanceRecursive(s1, this.rest(s2));
                distanceInsert = 1 + this.editDistanceRecursive(this.rest(s1), s2);
                return min(distanceNoOp, distanceCanc, distanceInsert);
            }
        }
        return -1;
    }

    /**
     * This method compute the edit distance between two strings s1 and s2
     *
     * @return the edit distance between s1 and s2 or -1 if s1 and s2 are both
     * null
     */
    public int editDistanceDin() {
        if (s1 != null && s2 != null) {
            if (s1.length() == 0 && s2.length() > 0) {
                return s2.length();
            } else if (s1.length() > 0 && s2.length() == 0) {
                return s1.length();
            } else if (s1.length() == s2.length()) {
                if (s1.equals(s2)) {
                    return 0;
                } else if (!s1.equals(s2)) {
                    int lcs = lengthOfLongestCommonSubsequence();
                    return (s2.length() - lcs) * 2;
                }
            } else if (s1.length() < s2.length()) {
                int lcs = lengthOfLongestCommonSubsequence();
                if (lcs == s1.length()) {
                    return (s2.length() - lcs);
                }
                // lcs < s1.length()
                return (s2.length() - s1.length() + (s1.length() - lcs) * 2);
            } else { // s1.length() > s2.length()
                int lcs = lengthOfLongestCommonSubsequence();
                if (lcs == s2.length()) {
                    return (s1.length() - lcs);
                }
                // lcs < s2.length()
                return (s1.length() - s2.length() + (s2.length() - lcs) * 2);
            }
        }
        return -1;
    }

    /**
     * This method computes the longest common subsequece between s1 and s2. A
     * common subsequence is intended as a common sequence of characters in the
     * two strings where the characters maintain the same relative order of the
     * two strings where they appear. The difference between two character index
     * can be more then 1. For example the longest common subsequence between
     * s1: "cara", s2: "casa" is "caa".
     *
     * @return the length of the longest common subsequence between s1 and s2
     */
    private int lengthOfLongestCommonSubsequence() {
        int lcsLength = 0;
        int[][] lcsMatrix = new int[s1.length() + 1][s2.length() + 1];
        for (int column = 0; column < s2.length() + 1; column++) {
            lcsMatrix[0][column] = 0;
        }
        for (int raw = 0; raw < s1.length() + 1; raw++) {
            lcsMatrix[raw][0] = 0;
        }
        for (int raw = 1; raw <= s1.length(); raw++) {
            for (int column = 1; column <= s2.length(); column++) {
                if (s1.charAt(raw - 1) == s2.charAt(column - 1))//match
                {
                    lcsMatrix[raw][column] = lcsMatrix[raw - 1][column - 1] + 1;
                } else { //no match
                    lcsMatrix[raw][column] = this.maxValue(lcsMatrix[raw][column - 1], lcsMatrix[raw - 1][column]);
                }
                lcsLength = this.maxValue(lcsMatrix[raw][column], lcsLength);
            }
        }
        return lcsLength;
    }

    /**
     * This method computes the bigger value between firstValue and secondValue
     *
     * @param firstValue: the value to compare with secondValue
     * @param secondValue: the value to compare with firstValue
     * @return the bigger value between firstValue and secondValue
     */
    private int maxValue(int firstValue, int secondValue) {
        if (firstValue > secondValue) {
            return firstValue;
        } else if (firstValue < secondValue) {
            return secondValue;
        } else {
            return firstValue;
        }
    }
    
    /**
     * This method computes the smaller value between firstValue and secondValue
     *
     * @param firstValue: the value to compare with secondValue
     * @param secondValue: the value to compare with firstValue
     * @return the smaller value between firstValue and secondValue
     */
    private int minValue(int firstValue, int secondValue) {
        if (firstValue < secondValue) {
            return firstValue;
        } else if (firstValue > secondValue) {
            return secondValue;
        } else {
            return firstValue;
        }
    }
    
    /**
     * This method computes the smaller value between distanceNoOp, distanceCanc
     * and distanceInsert.
     * The method uses the minValue method to determine the smaller between 
     * the first two parameters then it uses minValue again with the third 
     * parameter and the result of the first call of minValue.
     * 
     * 
     * @param distanceNoOp
     * @param distanceCanc
     * @param distanceInsert
     * @return the smaller value between distanceNoOp, distanceCanc, distanceInsert
     */
    private int min(int distanceNoOp, int distanceCanc, int distanceInsert) {
        return minValue(minValue(distanceNoOp, distanceCanc), distanceInsert);
    }
}
