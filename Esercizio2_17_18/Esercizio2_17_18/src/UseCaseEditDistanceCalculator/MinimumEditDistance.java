package UseCaseEditDistanceCalculator;

import esercizio2_17_18.EditDistanceCalculator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import javafx.util.Pair;

/**
 * This class implements an application that uses the editDistanceDin() method
 * to find for every string W in the correctme.txt file the list of strings,
 * stored in the dictionary.txt file, with minimum edit distance from W.
 *
 * @author soryt
 */
public class MinimumEditDistance {

    static String[] correctmeTxtFileStrings;
    static FileReader fr;
    static BufferedReader br;
    static List<Pair<String,Integer>> stringsWithEditDistance;
    
    /**
     * This method uses a Predicate object that needs to keep (if the Predicate is false)
     * or delete (if the Predicate is true) the elements inside the stringsWithEditDistance 
     * list whose edit distance is different from the min edit distance computed.
     *
     * @param minEditDistance: the min edit distance computed
     * @return a Predicate object
     */
    private static Predicate<Pair<String, Integer>> minEditDistanceIsDifferent(int minEditDistance) {
        return p -> p.getValue() != minEditDistance;
    }

    /**
     * This method find the list of Strings, stored in the dictionary.txt file,
     * with minimum edit distance from word
     *
     * @param word: the string on which the edit distance is compute
     */
    private static void minEditDistanceStringsList() throws FileNotFoundException, IOException {
        stringsWithEditDistance = new ArrayList();
        String line;
        EditDistanceCalculator edc = new EditDistanceCalculator();
        int minEditDistance = Integer.MAX_VALUE;
        for (String correctMeString : correctmeTxtFileStrings) {
            edc.setS2(correctMeString);
            fr = new FileReader("dictionary.csv");
            br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                edc.setS1(line);
                stringsWithEditDistance.add(new Pair(edc.getS1(), edc.editDistanceDin()));
                minEditDistance = min(minEditDistance, stringsWithEditDistance.get(stringsWithEditDistance.size() - 1).getValue());
            }
            stringsWithEditDistance.removeIf(minEditDistanceIsDifferent(minEditDistance));
            System.out.println("Min Edit_distance for string " + "\"" + edc.getS2() + "\"" + ": " + minEditDistance);
            System.out.println("List of min Edit_distance strings from string \"" + correctMeString + "\": ");
            stringsWithEditDistance.forEach((elem) -> {
                System.out.println("String \"" + elem.getKey() + "\" -> " + "Edit_distance: " + elem.getValue());
            });
            System.out.println("\n\n");
            stringsWithEditDistance.clear();
            minEditDistance = Integer.MAX_VALUE;
        }
        br.close();
        fr.close();
    }

    private static int min(int editDistance1, int editDistance2) {
        if (editDistance1 < editDistance2) {
            return editDistance1;
        } else if (editDistance1 > editDistance2) {
            return editDistance2;
        } else {
            return editDistance2;
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        String line;
        fr = new FileReader("correctme.txt");
        br = new BufferedReader(fr);
        line = br.readLine();
        line = line.replaceAll("[ \\p{Punct}]", "!");
        line = line.replaceAll("!!", "!");
        correctmeTxtFileStrings = line.split("!");
        br.close();
        fr.close();
        minEditDistanceStringsList();
    }
}
