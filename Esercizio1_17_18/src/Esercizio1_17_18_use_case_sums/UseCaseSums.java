package Esercizio1_17_18_use_case_sums;

import esercizio1_17_18.Couple;
import esercizio1_17_18.Sorter;
import esercizio1_17_18.SorterException;
import esercizio1_17_18.TypeComparator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * This class shows a possible use of the Sorter class in a real case. In this
 * use case the Sorter method findElementsWhoseSumIsEqualTo() is used to find
 * two elements in an array of 20 milions of elements, loaded from the file
 * integers.csv, whose sum is equal to the elements stored in the file sums.txt.
 * The file sums.txt contains 11 integers so the method will find (if it exists)
 * a couple of integers for all the 11 integers in the file.
 *
 *
 * @author soryt
 */
public class UseCaseSums {

    static List<BigInteger> array;

    static void readData() throws FileNotFoundException, IOException {
        try (FileReader fr = new FileReader("integers.csv"); BufferedReader br = new BufferedReader(fr)) {
            array = new ArrayList();
            String line;
            System.out.println("LOADING DATA FROM FILE integers.csv");
            long startTime = System.nanoTime();
            while ((line = br.readLine()) != null) {
                array.add(new BigInteger(line));
            }
            long endTime = System.nanoTime();
            System.out.println("LOADING COMPLETE IN " + TimeUnit.NANOSECONDS.toSeconds(endTime - startTime) + " SECONDS");

        }
    }

    public static void main(String[] args) throws SorterException, FileNotFoundException, IOException {
        readData();      
        
        Sorter sorter = new Sorter(new TypeComparator(), array);
        long startTime = System.nanoTime();
        System.out.println("SORTING DATA WITH MERGE SORT");
        sorter.mergeSort(Sorter.INCR);
        long endTime = System.nanoTime();
        System.out.println("SORTING COMPLETE IN " + TimeUnit.NANOSECONDS.toSeconds(endTime-startTime) + " SECONDS");
        try (FileReader fr = new FileReader("sums.txt"); BufferedReader br = new BufferedReader(fr)) {
            String line;         
            Couple couple;
            while((line = br.readLine())!= null){
                System.out.println("FINDING ELEMENTS WHOSE SUM IS EQUAL TO: " + line);
                startTime = System.nanoTime();
                couple = sorter.findElementsWhoseSumIsEqualTo(new BigInteger(line));
                endTime = System.nanoTime();
                if(couple.getFirst() > 0 && couple.getSecond() > 0)
                    System.out.println("ELEMENTS FOUND IN " + TimeUnit.NANOSECONDS.toSeconds(endTime-startTime) + " SECONDS" +
                            "\nFIRST ELEMENT: " + sorter.getArray().get(couple.getSecond()) + "\nSECOND ELEMENT: " + 
                            sorter.getArray().get(couple.getFirst()));
                            
                else
                    System.out.println("ELEMENT " + line + " NOT FOUND");
            }
        }
    }
}
