package esercizio1_17_18;

/**
 * This class defines the object Couple whose aim is to store two int values. It's
 * used in the method Sorter.findElementsWhoseSumIsEqualTo(BigInteger sum) to memorize
 * and return the two indexes of the elements which sum is equal to sum
 * @author soryt
 */
public class Couple {   
    private final int first;
    private final int second;
    

    public Couple(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }  
}
