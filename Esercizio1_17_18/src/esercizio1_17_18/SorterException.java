package esercizio1_17_18;

/**
 *
 * @author soryt
 */
public class SorterException extends Exception{
    /**
     * 
     * @param message: message displayed when the exception is thrown 
     */
    public SorterException(String message) {
     super(message);
    }
}
