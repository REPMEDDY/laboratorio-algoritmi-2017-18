package esercizio1_17_18;
import java.util.Comparator;
public class TypeComparator<T extends Comparable<T>> implements Comparator<T>{
    @Override
    public int compare(T el1, T el2){

        int result;
        
        if(el1.compareTo(el2) < 0){
            result = -1;
        }else if(el1.compareTo(el2) > 0)
            result = 1;
        else
            result = 0;
        return result;
    }
}