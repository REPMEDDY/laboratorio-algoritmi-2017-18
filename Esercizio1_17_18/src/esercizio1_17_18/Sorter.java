package esercizio1_17_18;

import java.lang.annotation.Native;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * This class provides methods to sort an array of elements of type T
 *
 * @author soryt
 * @param <T> the type of the element stored in the array
 */
public class Sorter<T> {

    /**
     * Specify that the array will be sorted in decreasing order
     */
    @Native public static final int DECR = 0;
    /**
     * Specify that the array will be sorted in increasing order
     */
    @Native public static final int INCR = 1;
    private int firstHalfArrayIndex;
    private int secondHalfArrayIndex;
    private List<T> array;
    private List<T> arraySupport;
    private Comparator<? super T> comparator;

    /**
     *
     * @return
     */
    public List<T> getArray() {
        return array;
    }

    public List<T> getArraySupport() {
        return arraySupport;
    }

    public Sorter(Comparator<? super T> comparator, List<T> array) {
        this.comparator = comparator;
        this.array = array;
    }

    /**
     * Adds an element to the array
     *
     * @param element: the element to add
     */
    public void add(T element) {
        this.array.add(element);
    }//add

    /**
     *
     * @return true if and only if this ordered array is empty
     */
    public boolean isEmpty() {
        return (this.array).isEmpty();
    }//isEmpty

    /**
     * @return the number of elements currently stored in the ordered array
     */
    public int size() {
        return (this.array).size();
    }//size

    /**
     * Calls the compare method of the comparator
     *
     * @param element1: the first element to compare
     * @param element2: the second element to compare
     * @return a value greater, less or equal to zero according to the
     * comparator.compare() method
     */
    public int compareElements(T element1, T element2) {
        return this.comparator.compare(element1, element2);
    }//compareElements

    /**
     * Sort the array using the insertion sort algorithm
     *
     * @param TYPE_OF_ORDER: specifies if the array will be sorted in decreasing
     * (DECR) or increasing order (INCR)
     * @throws esercizio1_17_18.SorterException if the array is null
     */
    public void insertionSort(int TYPE_OF_ORDER) throws SorterException {
        if (this.array == null) {
            throw new SorterException("insertionSort(): array cannot be null");
        } else {
            if (!this.array.isEmpty()) {
                int end, init;
                T temp;
                // Sort list[] into increasing order.
                for (end = 1; end < array.size(); end++) {
                    temp = array.get(end);

                    if (TYPE_OF_ORDER == Sorter.INCR) {
                        for (init = end; init > 0 && compareElements(array.get(init - 1), temp) > 0; init--) {
                            array.set(init, array.get(init - 1));
                        }
                    } else {
                        for (init = end; init > 0 && compareElements(array.get(init - 1), temp) <= 0; init--) {
                            array.set(init, array.get(init - 1));
                        }
                    }
                    array.set(init, temp);
                }
            }
        }
    }//insertionSort

    /**
     * Calls the merge sort algorithm
     *
     * @param TYPE_OF_ORDER: specifies if the array will be sorted in decreasing
     * (DECR) or increasing order (INCR)
     * @throws esercizio1_17_18.SorterException if the array is null
     */
    public void mergeSort(int TYPE_OF_ORDER) throws SorterException {
        if (array == null) {
            throw new SorterException("mergeSort(): array cannot be null");
        }
        if (array.size() > 1) {
            this.arraySupport = new ArrayList();
            mergeSortSupport(TYPE_OF_ORDER, 0, this.array.size() - 1);
            this.array = this.arraySupport;
            this.arraySupport = null;
        }

    }//mergeSort

    /**
     * The support method of mergeSort that splits the given array in two parts
     * (almost equal in general) for the first time
     *
     * @param TYPE_OF_ORDER: specifies if the array will be sorted in decreasing
     * (DECR) or increasing order (INCR)
     * @param begin: the index of the first element in the array
     * @param end: the index of the last element in the array
     */
    private void mergeSortSupport(int TYPE_OF_ORDER, int begin, int end) {
        if (!this.array.isEmpty() && this.array.size() > 1) {
            int half = (begin + end) / 2;
            List<T> firstHalfArray = sort(TYPE_OF_ORDER, begin, half);
            List<T> secondHalfArray = sort(TYPE_OF_ORDER, half + 1, end);
            this.arraySupport = merge(TYPE_OF_ORDER, firstHalfArray, secondHalfArray);
        }
    }//mergeSortSupport

    /**
     * Recursively splits in two parts (almost equal in general) the array to
     * allow the sorting
     *
     * @param TYPE_OF_ORDER: specifies if the array will be sorted in decreasing
     * (DECR) or increasing order (INCR)
     * @param begin: the index of the first element in the array
     * @param end: the index of the last element in the array
     * @return a sorted array
     */
    private List<T> sort(int TYPE_OF_ORDER, int begin, int end) {
        List<T> arraySorted = new ArrayList();
        if (begin == end) {
            arraySorted.add(array.get(begin));
            return arraySorted;
        } else {
            if (begin < end && (end == begin + 1)) {
                if (TYPE_OF_ORDER == Sorter.INCR) {
                    return sortIncr(arraySorted, begin, end);
                } else {
                    return sortDecr(arraySorted, begin, end);
                }
            } else {
                if (begin < end && (end > begin + 1)) {
                    int half = (begin + end) / 2;
                    List<T> firstHalfArray = sort(TYPE_OF_ORDER, begin, half);
                    List<T> secondHalfArray = sort(TYPE_OF_ORDER, half + 1, end);
                    arraySorted = merge(TYPE_OF_ORDER, firstHalfArray, secondHalfArray);
                }
            }
        }
        return arraySorted;
    }//sort

    /**
     * Sort the portion of the array between begin and end in increasing order
     *
     * @param arraySorted: the array to sort
     * @param begin: index of the first element in the array
     * @param end: index of the last element in the array
     * @return the array sorted in increasing order
     */
    private List<T> sortIncr(List<T> arraySorted, int begin, int end) {
        if (this.compareElements(this.array.get(begin), this.array.get(end)) > 0) {
            arraySorted.add(this.array.get(end));
            arraySorted.add(this.array.get(begin));
            return arraySorted;
        } else {
            arraySorted.add(this.array.get(begin));
            arraySorted.add(this.array.get(end));
            return arraySorted;
        }
    }//sortIncr

    /**
     * Sort the portion of the array between begin and end in decreasing order
     *
     * @param arraySorted: the array to sort
     * @param begin: index of the first element in the array
     * @param end: index of the last element in the array
     * @return the array sorted in decreasing order
     */
    private List<T> sortDecr(List<T> arraySorted, int begin, int end) {
        if (this.compareElements(this.array.get(begin), this.array.get(end)) > 0) {
            arraySorted.add(this.array.get(begin));
            arraySorted.add(this.array.get(end));
            return arraySorted;
        } else {
            arraySorted.add(this.array.get(end));
            arraySorted.add(this.array.get(begin));
            return arraySorted;
        }
    }//sortDecr

    /**
     * Merge two ordered array in a new array still ordered
     *
     * @param TYPE_OF_ORDER: specifies if the array will be sorted in decreasing
     * (DECR) or increasing order (INCR)
     * @param firstHalfArray: one of the two array to merge
     * @param secondHalfArray: one of the two array to merge
     * @return a ordered array that contains the elements of the two array
     * merged
     */
    private List<T> merge(int TYPE_OF_ORDER, List<T> firstHalfArray, List<T> secondHalfArray) {
        List<T> mergedArraySorted = new ArrayList();
        firstHalfArrayIndex = 1;
        secondHalfArrayIndex = 1;
        boolean firstHalfIndexInBound = true;
        boolean secondHalfIndexInBound = true;
        for (int i = 0; (i < (firstHalfArray.size() + secondHalfArray.size())) && (firstHalfIndexInBound && secondHalfIndexInBound); i++) {
            if (TYPE_OF_ORDER == Sorter.INCR) {
                mergedArraySorted.addAll(mergeIncr(firstHalfArray, secondHalfArray));
            } else {
                mergedArraySorted.addAll(mergeDecr(firstHalfArray, secondHalfArray));
            }
            if (firstHalfArrayIndex > firstHalfArray.size()) {
                firstHalfIndexInBound = false;
            }
            if (secondHalfArrayIndex > secondHalfArray.size()) {
                secondHalfIndexInBound = false;
            }
        }
        if (firstHalfIndexInBound == false && secondHalfIndexInBound == true) {
            mergedArraySorted.addAll(secondHalfArray.subList(secondHalfArrayIndex - 1, secondHalfArray.size()));
        } else {
            mergedArraySorted.addAll(firstHalfArray.subList(firstHalfArrayIndex - 1, firstHalfArray.size()));
        }
        return mergedArraySorted;
    }//merge

    /**
     * Merge two array in increasing order
     *
     * @param firstHalfArray: one of the two array to merge
     * @param secondHalfArray: one of the two array to merge
     * @return a ordered array that contains the elements of the two array
     * merged
     */
    private List<T> mergeIncr(List<T> firstHalfArray, List<T> secondHalfArray) {
        List<T> mergedArraySortedIncr = new ArrayList();
        if (this.compareElements(firstHalfArray.get(firstHalfArrayIndex - 1), secondHalfArray.get(secondHalfArrayIndex - 1)) < 0) {
            mergedArraySortedIncr.add(firstHalfArray.get(firstHalfArrayIndex - 1));
            firstHalfArrayIndex++;
        } else if (this.compareElements(firstHalfArray.get(firstHalfArrayIndex - 1), secondHalfArray.get(secondHalfArrayIndex - 1)) > 0) {
            mergedArraySortedIncr.add(secondHalfArray.get(secondHalfArrayIndex - 1));
            secondHalfArrayIndex++;
        } else {
            mergedArraySortedIncr.add(firstHalfArray.get(firstHalfArrayIndex - 1));
            mergedArraySortedIncr.add(secondHalfArray.get(secondHalfArrayIndex - 1));
            firstHalfArrayIndex++;
            secondHalfArrayIndex++;
        }
        return mergedArraySortedIncr;
    }//mergeIncr

    /**
     * Merge two array in decreasing order
     *
     * @param firstHalfArray: one of the two array to merge
     * @param secondHalfArray: one of the two array to merge
     * @return a ordered array that contains the elements of the two array
     * merged
     */
    private List<T> mergeDecr(List<T> firstHalfArray, List<T> secondHalfArray) {
        List<T> mergedArraySortedDecr = new ArrayList();
        if (this.compareElements(firstHalfArray.get(firstHalfArrayIndex - 1), secondHalfArray.get(secondHalfArrayIndex - 1)) < 0) {
            mergedArraySortedDecr.add(secondHalfArray.get(secondHalfArrayIndex - 1));
            secondHalfArrayIndex++;
        } else if (this.compareElements(firstHalfArray.get(firstHalfArrayIndex - 1), secondHalfArray.get(secondHalfArrayIndex - 1)) > 0) {
            mergedArraySortedDecr.add(firstHalfArray.get(firstHalfArrayIndex - 1));
            firstHalfArrayIndex++;
        } else {
            mergedArraySortedDecr.add(firstHalfArray.get(firstHalfArrayIndex - 1));
            mergedArraySortedDecr.add(secondHalfArray.get(secondHalfArrayIndex - 1));
            firstHalfArrayIndex++;
            secondHalfArrayIndex++;
        }
        return mergedArraySortedDecr;
    }//mergeDecr

    /**
     * Find, if they exists, two elements in the array whose sum is equal to the value passed
     * by parameter. The method uses the binary search to find the difference
     * between sum and currEl in the array so that difference + currEl = sum
     *
     * @param sum: the element on which the research is done
     * @return a Couple object that stores the indexes of the first two elements
     * found whose sum is equal to element else return a Couple object that
     * stores negative indexes meaning that there are no elements in the array
     * whose sum is equal to element
     * @throws esercizio1_17_18.SorterException if the array is null or if the
     * array contains elements that are not integers
     */
    public Couple findElementsWhoseSumIsEqualTo(BigInteger sum) throws SorterException {
        if (this.array == null) {
            throw new SorterException("findElementsWhoseSumIsEqualTo(): array cannot be null");
        }
        if (this.array.get(0) instanceof BigInteger == false) {
            throw new SorterException("findElementsWhoseSumIsEqualTo(): array of Integer required");
        }
        if (!this.array.isEmpty() && this.array.size() > 1) {
            //mergeSort(Sorter.INCR);
            int found = -1;
            int i = 0;
            BigInteger currEl;
            while (i < this.array.size() && found == -1) {
                currEl = (BigInteger) this.array.get(i);
                found = binarySearch(sum.subtract(currEl), 0, array.size() - 1);
                if (found == -1) {
                    i++;
                }
            }
            if (found != -1) {
                return new Couple(i, found);
            } else {
                return new Couple(-1, -1);
            }
        } else {
            return new Couple(-1, -1);
        }
    }

    /**
     * Search the element passed has parameter in the array
     *
     * @param difference: the element to search
     * @param begin: the index of the first element in the sub-array
     * @param end: the index of the last element in the sub-array
     * @return the index of the element if the element is in the array, -1
     * otherwise
     */
    private int binarySearch(BigInteger difference, int begin, int end) {
        if (begin <= end) {
            int half = (begin + end) / 2;
            if (this.compareElements((T) difference, this.array.get(half)) < 0) {
                return binarySearch(difference, begin, half - 1);
            } else if (this.compareElements((T) difference, this.array.get(half)) > 0) {
                return binarySearch(difference, half + 1, end);
            } else if (this.compareElements((T) difference, this.array.get(half)) == 0) {
                return half;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

}
