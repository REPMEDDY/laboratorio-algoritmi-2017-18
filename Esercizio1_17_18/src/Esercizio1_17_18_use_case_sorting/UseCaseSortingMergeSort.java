package Esercizio1_17_18_use_case_sorting;

import esercizio1_17_18.Sorter;
import esercizio1_17_18.SorterException;
import esercizio1_17_18.TypeComparator;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
/**
 * This class shows a possible use of the Sorter class in a real case. In this
 * use case the Sorter method mergeSort() is used to sort an array of 20
 * milions of elements loaded from the file integers.csv
 * 
 * @author soryt
 */
public class UseCaseSortingMergeSort {
    static List array = new ArrayList<BigInteger>();
    
    static void readData() throws FileNotFoundException, IOException{
        try (FileReader fr = new FileReader("integers.csv"); BufferedReader br = new BufferedReader(fr)) {
            String line;
            System.out.println("LOADING DATA FROM FILE");
            long startTime = System.nanoTime();
            while((line = br.readLine()) != null){
                array.add(new BigInteger(line));
            }
            long endTime = System.nanoTime();
            System.out.println("LOADING COMPLETE IN " + TimeUnit.NANOSECONDS.toSeconds(endTime - startTime) + " SECONDS");
            
        }
    }
    
    public static void main(String [] args) throws SorterException, IOException{
        UseCaseSortingMergeSort.readData();
        Sorter sorter = new Sorter(new TypeComparator(),array);
        System.out.println("SORTING DATA WITH MERGE SORT");
        long startTime = System.nanoTime();
        sorter.mergeSort(Sorter.INCR);
        long endTime = System.nanoTime();
        System.out.println("SORTING COMPLETE IN " + TimeUnit.NANOSECONDS.toSeconds(endTime - startTime) + " SECONDS");       
        System.out.println("CHECKING IF THE SORTING IS CORRECT");
        boolean sortingIsCorrect = true;
        int i = 0;
        startTime = System.nanoTime();
        while(sortingIsCorrect && i < sorter.getArray().size()-1){
            if(sorter.compareElements(sorter.getArray().get(i), sorter.getArray().get(i+1))<=0)
                i++;
            else
                sortingIsCorrect = false;
        }
        endTime = System.nanoTime();
        if(sortingIsCorrect)
            System.out.println("THE SORTING IS CORRECT\nCHECKING TIME: " + TimeUnit.NANOSECONDS.toSeconds(endTime - startTime) + " SECONDS");
        else
            System.out.println("THE SORTING IS WRONG\nCHECKING TIME: " + TimeUnit.NANOSECONDS.toSeconds(endTime - startTime) + " SECONDS");
    }
}
