/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import esercizio1_17_18.Sorter;
import esercizio1_17_18.SorterException;
import esercizio1_17_18.TypeComparator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class SorterTestingsMergeSort {
    
    Sorter sorter1; //testOnArrayWithAllEqualElementsIncr()
    Sorter sorter2; //testOnArrayWithAllEqualElementsDecr()
    Sorter sorter3; //testOnIntegerArrayIncr()
    Sorter sorter4; //testOnIntegerArrayDecr()
    Sorter sorter5; //testOnEmptyArray()
    Sorter sorter6; //testOnAlreadyOrderedArrayIncr()
    Sorter sorter7; //testOnAlreadyOrderedArrayDecr()
    Sorter sorter8; //testOnArrayWithOneElement()
    Sorter sorterCSV;
    FileReader fr;
    BufferedReader br;
    Comparator comparator;
    
    //Start test
    @Before
    public void setUpTestOnArrayWithAllEqualElementsIncr(){
        List<Integer> arrayEqualElements = new ArrayList();
        arrayEqualElements.add(3);
        arrayEqualElements.add(3);
        arrayEqualElements.add(3);
        arrayEqualElements.add(3);
        arrayEqualElements.add(3);
        comparator = new TypeComparator();
        sorter1 = new Sorter(comparator, arrayEqualElements); 
    }  
    @Test
    public void testOnArrayWithAllEqualElementsIncr() throws SorterException{
        sorter1.mergeSort(Sorter.INCR);
        for (int i = 1; i < sorter1.getArray().size(); i++) {
            assertTrue(sorter1.compareElements(sorter1.getArray().get(i - 1), sorter1.getArray().get(i)) == 0);
        }          
    }
    //End test
    
    //Start test
    @Before 
    public void setUpTestOnArrayWithAllEqualElementsDecr(){
        List<Integer> arrayEqualElements = new ArrayList();
        arrayEqualElements.add(3);
        arrayEqualElements.add(3);
        arrayEqualElements.add(3);
        arrayEqualElements.add(3);
        arrayEqualElements.add(3);
        comparator = new TypeComparator();
        sorter2 = new Sorter(comparator, arrayEqualElements);
    }
    @Test
    public void testOnArrayWithAllEqualElementsDecr() throws SorterException{
        sorter2.mergeSort(Sorter.DECR);
        for (int i = 1; i < sorter2.getArray().size(); i++) {
            assertTrue(sorter2.compareElements(sorter2.getArray().get(i - 1), sorter2.getArray().get(i)) == 0);
        }
    }
    //End test

    //Start test
    @Before
    public void setUpTestOnIntegerArrayIncr() {
        List<Integer> array = new ArrayList();
        array.add(8);
        array.add(1);
        array.add(12);
        array.add(2);
        array.add(3);
        comparator = new TypeComparator();
        sorter3 = new Sorter(comparator, array);
    }

    @Test
    public void testOnIntegerArrayIncr() throws SorterException {
        sorter3.mergeSort(Sorter.INCR);
        for (int i = 1; i < sorter3.getArray().size(); i++) {
            assertTrue(sorter3.compareElements(sorter3.getArray().get(i-1), sorter3.getArray().get(i)) <= 0);
        }
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestsIntegerArrayDecr() {
        List<Integer> array = new ArrayList();
        array.add(8);
        array.add(1);
        array.add(12);
        array.add(2);
        array.add(3);
        comparator = new TypeComparator();
        sorter4 = new Sorter(comparator, array);                  
    } 
    @Test
    public void testOnIntegerArrayDecr() throws SorterException {
        sorter4.mergeSort(Sorter.DECR);      
        for (int i = 1; i < sorter4.getArray().size(); i++) {
            assertTrue(sorter4.compareElements(sorter4.getArray().get(i - 1), sorter4.getArray().get(i)) >= 0);
        }       
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestOnEmptyArray(){
        List<Integer> arrayEmpty = new ArrayList();
        comparator = new TypeComparator();
        sorter5 = new Sorter(comparator, arrayEmpty);
    }
    @Test
    public void testOnEmptyArray() throws SorterException{
        sorter5.mergeSort(Sorter.INCR);
        assertTrue(sorter5.isEmpty() == true);    
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestOnAlreadyOrderedArrayIncr(){
        List<Integer> arrayAlreadyOrderedIncr = new ArrayList();
        arrayAlreadyOrderedIncr.add(1);
        arrayAlreadyOrderedIncr.add(8);
        arrayAlreadyOrderedIncr.add(12);
        arrayAlreadyOrderedIncr.add(45);
        arrayAlreadyOrderedIncr.add(46);
        comparator = new TypeComparator();
        sorter6 = new Sorter(comparator, arrayAlreadyOrderedIncr); 
    }
    @Test
    public void testOnAlreadyOrderedArrayIncr() throws SorterException{
        sorter6.mergeSort(Sorter.INCR);
        for (int i = 1; i < sorter6.getArray().size(); i++) {
            assertTrue(sorter6.compareElements(sorter6.getArray().get(i - 1), sorter6.getArray().get(i)) <= 0);
        }      
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestOnAlreadyOrderedArrayDecr(){
        List<Integer> arrayAlreadyOrderedDecr = new ArrayList();
        arrayAlreadyOrderedDecr.add(46);
        arrayAlreadyOrderedDecr.add(45);
        arrayAlreadyOrderedDecr.add(12);
        arrayAlreadyOrderedDecr.add(8);
        arrayAlreadyOrderedDecr.add(1);
        comparator = new TypeComparator();
        sorter7 = new Sorter(comparator, arrayAlreadyOrderedDecr); 
    }
    @Test
    public void testOnAlreadyOrderedArrayDecr() throws SorterException{
        sorter7.mergeSort(Sorter.DECR);
        for (int i = 1; i < sorter7.getArray().size(); i++) {
            assertTrue(sorter7.compareElements(sorter7.getArray().get(i - 1), sorter7.getArray().get(i)) >= 0);
        }        
    }
    //End test
    
    //Start test
    @Before
    public void setUpTestOnArrayWithOneElement(){
        List<Integer> arrayOneElement = new ArrayList();
        arrayOneElement.add(2);
        comparator = new TypeComparator();
        sorter8 = new Sorter(comparator, arrayOneElement);
    }
    @Test
    public void testOnArrayWithOneElement() throws SorterException {
        sorter8.mergeSort(Sorter.INCR);
        assertTrue(sorter8.compareElements(sorter8.getArray().get(0),sorter8.getArray().get(0) ) == 0);
    }
    //End test

    
    //Start test
    @Before
    public void setUpTestOnCSVMergeSort() throws FileNotFoundException, IOException {
        fr = new FileReader("integers.csv");
        br = new BufferedReader(fr);
        comparator = new TypeComparator();
        ArrayList<BigDecimal> array = new ArrayList();
        sorterCSV = new Sorter(comparator, array);
        String line;
        while ((line = br.readLine()) != null) {
            sorterCSV.add(new BigDecimal(line));
        }
        System.out.println("dimensione array CSV before:" + sorterCSV.getArray().size());
    }

    @Test
    public void testOnCSVMergeSortIncr() throws SorterException {
        sorterCSV.mergeSort(Sorter.INCR);
        for (int i = 1; i < sorterCSV.getArray().size(); i++) {
            assertTrue(sorterCSV.compareElements(sorterCSV.getArray().get(i - 1), sorterCSV.getArray().get(i)) <= 0);
        }
        System.out.println("dimensione array CSV test:" + sorterCSV.getArray().size());
    }
    //End test

    /*@Test
    public void testOnCSVMergeSortDecr() throws SorterException {
        sorterCSV.mergeSort(Sorter.DECR);
        for (int i = 1; i < sorterCSV.getArray().size(); i++) {
            assertTrue(sorterCSV.compareElements(sorterCSV.getArray().get(i - 1), sorterCSV.getArray().get(i)) >= 0);
        }
        System.out.println("dimensione array CSV test:" + sorterCSV.getArray().size());
    }
    //End test*/
}
