package esercizio3_17_18;

/**
 *
 * @author soryt
 */
public class PriorityQueueException extends Exception {
    
    /**
     * 
     * @param message: message displayed when the exception is thrown 
     */
    public PriorityQueueException(String message){
        super(message);
    }
}
