package esercizio3_17_18;

/**
 * This class represent the elements stored in the priority queue data
 * structure. Every element is a pair of two fields: priority and value
 *
 * @author soryt
 * @param <P>: the type of the priority of the element
 * @param <V>: the type of the value of the element
 */
public class QueueElem<P extends Comparable,V extends Comparable> {

    private int queueElemIndex;
    private double queueElemId;
    private P priority;
    private V value;
    
    public double getQueueElemId() {
        return queueElemId;
    }

    public void setQueueElemId(int queueElemId) {
        this.queueElemId = queueElemId;
    }
    
    public QueueElem(P priority, V value) {
        this.priority = priority;
        this.value = value;
    }

    public P getPriority() {
        return priority;
    }

    public void setPriority(P priority) {
        this.priority = priority;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public int getQueueElemIndex() {
        return queueElemIndex;
    }

    public void setQueueElemIndex(int queueElemIndex) {
        this.queueElemIndex = queueElemIndex;
    }

}
