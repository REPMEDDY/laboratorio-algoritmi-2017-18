package esercizio3_17_18;

import java.lang.annotation.Native;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javafx.util.Pair;

/**
 * This class represents the priority queue data structure. It uses a heap to
 * memorize the elements and the comparator allows to specify how to compare the
 * elements within the priority queue.
 *
 * @author soryt
 * @param <P>: the type of the priority
 */
public class PriorityQueue<P extends Comparable, V extends Comparable> {

    private final Heap heap;
    private final Comparator comparator;
    /**
     * Specify that the priority queue will be a max priority queue
     */
    @Native
    public static final int MAX_PRIORITY_QUEUE = 0;
    /**
     * Specify that the priority queue will be a min priority queue
     */
    @Native
    public static final int MIN_PRIORITY_QUEUE = 1;
    private final int priorityQueueType;

    /**
     *
     * @param comparator:allows to specify how to compare the elements within
     * the priority queue
     * @param priorityQueueType:specify if the priority queue will be a max or
     * min priority queue
     */
    public PriorityQueue(Comparator comparator, int priorityQueueType) {
        this.heap = new Heap();
        this.comparator = comparator;
        this.priorityQueueType = priorityQueueType;
    }

    /**
     *
     * Prints the priority of all the elements stored in the priority queue
     */
    public void print() {
        this.heap.heap.forEach((elem) -> {
            System.out.print(elem.getPriority() + " ");
        });
    }

    /**
     * Search all the elements in the priority queue with the same priority
     *
     * @param priority: the priority to search
     * @return a list of Pair where each Pair object is composed by a QueueElem
     * and the index at which the QueueElem is stored in the priority queue
     */
    public List<Pair<QueueElem, Integer>> search(P priority) throws PriorityQueueException {
        if (this != null) {
            List<Pair<QueueElem, Integer>> searchList = new ArrayList();
            int index = 0;
            while (index < this.getNumberOfElements() && this.comparator.compare(this.heap.heap.get(index).getPriority(), priority) != 0) {
                index++;
            }
            while (index <= this.getNumberOfElements() - 1 && this.comparator.compare(this.heap.heap.get(index).getPriority(), priority) == 0) {
                searchList.add(new Pair(this.heap.heap.get(index), index));
                index++;
            }
            return searchList;
        } else {
            throw new PriorityQueueException("Priority queue is null");
        }
    }

    /**
     * Check if the QueueElem is stored in the priority queue
     *
     * @param queueElemId: the id of the QueueElem to check
     * @return true if the QueueElem is stored in the priority queue, false
     * otherwise
     */
    public boolean isStoredInQueue(double queueElemId) {
        for (QueueElem elem : this.heap.heap) {
            if (this.comparator.compare(elem.getQueueElemId(), queueElemId) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the QueueElem is stored in the priority queue
     *
     * @param index: the index of the QueueElem to check
     * @return the QueueElem if it is stored in the priority queue, null
     * otherwise
     */
    public QueueElem getQueueElem(int index) {
        for (QueueElem queueElem : this.heap.heap) {
            if (((Integer) (queueElem.getValue())) == index) {
                return queueElem;
            }
        }
        return null;
    }

    /**
     * Check if the QueueElem is stored in the priority queue
     *
     * @param queueElemId: the id of the QueueElem to check
     * @return the QueueElem if it is stored in the priority queue, null
     * otherwise
     */
    public QueueElem getQueueElemById(double queueElemId) {
        for (QueueElem elem : this.heap.heap) {
            if (this.comparator.compare(elem.getQueueElemId(), queueElemId) == 0) {
                return elem;
            }
        }
        return null;
    }

    /**
     *
     * @return the number of elements stored in the priority queue
     */
    public int getNumberOfElements() {
        return this.heap.heap.size();
    }

    /**
     *
     * @return a list containing the elements stored in the priority queue
     */
    public List<QueueElem> getPriorityQueueElements() {
        return this.heap.heap;
    }

    /**
     * This class represent the data structure heap. A heap is an array of
     * elements that can be thought as a binary tree. A heap can be max heap of
     * min heap. In a max heap the greatest element is stored in the root and
     * every element stored in a node is greater or equal to his direct left and
     * right child. The min heap is the opposite of the max heap.
     *
     * Given indexCurrElem the index of the current element, the index of his
     * parent,left child and right child is respectively: - left:
     * (2*indexCurrElem) +1 - right: (2*indexCurrElem) +2 - parent:
     * indexCurrElem/2
     *
     * @author soryt
     */
    private class Heap {

        public final List<QueueElem> heap;

        private Heap() {
            this.heap = new ArrayList<>();
        }

        /**
         * Check if the element has one or both left and right child
         *
         * @param indexCurrElem: the index of the current element
         * @return true if the element has no left and right child, false
         * otherwise
         */
        private boolean isLeaf(int indexCurrElem) throws PriorityQueueException {
            return !(this.getLeft(indexCurrElem) >= 0 && this.getLeft(indexCurrElem) < this.heap.size());
        }

        /**
         * Compute the index of the left child
         *
         * @param indexCurrElem: the index of the current element
         * @return the index of his left child if exists
         */
        private int getLeft(int indexCurrElem) throws PriorityQueueException {
            int leftIndex = (2 * indexCurrElem) + 1;
            // if(leftIndex >= 0 && leftIndex < this.heap.size())
            return leftIndex;
            /*else
                throw new PriorityQueueException("Index out of bound: index= " + leftIndex + " size= " + this.heap.size());*/
        }

        /**
         * Compute the index of the right child
         *
         * @param indexCurrElem: the index of the current element
         * @return the index of his right child if exists
         */
        private int getRight(int indexCurrElem) {
            return (2 * indexCurrElem) + 2;
        }

        /**
         * Compute the index of the parent element
         *
         * @param indexCurrElem: the index of the current element
         * @return the index of the parent element
         */
        private int getParent(int indexCurrElem) {
            return indexCurrElem / 2;
        }

        /**
         * Ensure that the max heap property is maintained. The max heap
         * property is the following: every element is greater or equal to his
         * direct left and right child
         *
         * @param indexCurrElem: the index of the current element
         */
        private void maxHeapify(int indexCurrElem) throws PriorityQueueException {
            if (this.isLeaf(indexCurrElem) == false) {
                int leftIndexElem = this.getLeft(indexCurrElem);
                int rightIndexElem = this.getRight(indexCurrElem);
                int maxIndexElem = indexCurrElem;
                if (leftIndexElem < this.heap.size() && (comparator.compare(this.heap.get(leftIndexElem).getPriority(), this.heap.get(indexCurrElem).getPriority()) > 0)) {
                    maxIndexElem = leftIndexElem;
                }

                if (rightIndexElem < this.heap.size() && comparator.compare(this.heap.get(rightIndexElem).getPriority(), this.heap.get(maxIndexElem).getPriority()) > 0) {
                    maxIndexElem = rightIndexElem;
                }

                if (maxIndexElem != indexCurrElem) {
                    this.swap(indexCurrElem, maxIndexElem);
                    maxHeapify(maxIndexElem);
                }
            }
        }

        /**
         * Ensure that the min heap property is maintained. The min heap
         * property is the following: every element is less or equal to his
         * direct left and right child
         *
         * @param indexCurrElem: the index of the current element
         */
        private void minHeapify(int indexCurrElem) throws PriorityQueueException {
            if (this.isLeaf(indexCurrElem) == false) {
                int leftIndexElem = this.getLeft(indexCurrElem);
                int rightIndexElem = this.getRight(indexCurrElem);
                int minIndexElem = indexCurrElem;
                if (leftIndexElem < this.heap.size() && (comparator.compare(this.heap.get(leftIndexElem).getPriority(), this.heap.get(indexCurrElem).getPriority()) < 0)) {
                    minIndexElem = leftIndexElem;
                }

                if (rightIndexElem < this.heap.size() && comparator.compare(this.heap.get(rightIndexElem).getPriority(), this.heap.get(minIndexElem).getPriority()) < 0) {
                    minIndexElem = rightIndexElem;
                }

                if (minIndexElem != indexCurrElem) {
                    this.swap(indexCurrElem, minIndexElem);
                    minHeapify(minIndexElem);
                }
            }
        }

        /**
         * Places the element in indexCurrElem position at maxIndexElem and the
         * element in maxIndexElem at indexCurrElem
         *
         * @param indexCurrElem: the element at indexCurrElem position
         * @param maxIndexElem: the element at maxIndexElem position
         */
        private void swap(int indexCurrElem, int maxIndexElem) {
            QueueElem lower = this.heap.get(indexCurrElem);
            QueueElem higher = this.heap.get(maxIndexElem);
            lower.setQueueElemIndex(maxIndexElem);
            higher.setQueueElemIndex(indexCurrElem);
            this.heap.set(indexCurrElem, higher);
            this.heap.set(maxIndexElem, lower);
        }
    }//end private class Heap

    /**
     *
     * @return the element with the highest priority in the priority queue
     */
    public QueueElem maximum() {
        return this.heap.heap.get(0);
    }

    /**
     *
     * @return the element with the lowest priority in the priority queue
     */
    public QueueElem minimum() {
        return this.heap.heap.get(0);
    }

    /**
     * Remove the element with the highest priority in the priority queue
     *
     * @return the element removed
     * @throws PriorityQueueException if the heap has less then 1 element
     */
    public QueueElem extractMax() throws PriorityQueueException {
        if (this.priorityQueueType == PriorityQueue.MIN_PRIORITY_QUEUE) {
            throw new PriorityQueueException("Cannot get min priority element in a max priority queue");
        } else if (this.heap.heap.size() < 0) {
            throw new PriorityQueueException("Priority queue underflow");
        } else {
            QueueElem maxElem = this.heap.heap.get(0);
            this.heap.heap.set(0, this.heap.heap.get(this.heap.heap.size() - 1));
            this.heap.heap.get(0).setQueueElemIndex(0);
            this.heap.heap.remove(this.heap.heap.size() - 1);
            this.heap.maxHeapify(0);
            return maxElem;
        }
    }

    /**
     * Remove the element with the lowest priority in the priority queue
     *
     * @return the element removed
     * @throws PriorityQueueException if the heap has less then 1 element
     */
    public QueueElem extractMin() throws PriorityQueueException {
        if (this.priorityQueueType == PriorityQueue.MAX_PRIORITY_QUEUE) {
            throw new PriorityQueueException("Cannot get max priority element in a min priority queue");
        } else if (this.heap.heap.size() < 0) {
            throw new PriorityQueueException("Priority queue underflow");
        } else {
            QueueElem minElem = this.heap.heap.get(0);
            this.heap.heap.set(0, this.heap.heap.get(this.heap.heap.size() - 1));
            this.heap.heap.get(0).setQueueElemIndex(0);
            this.heap.heap.remove(this.heap.heap.size() - 1);
            this.heap.minHeapify(0);
            return minElem;
        }
    }

    /**
     * Increase the priority of the element
     *
     * @param elem: the elem whose priority is increased
     * @param indexElem: the index at which the element is stored in the heap
     * @param priority: the new priority of the element
     * @throws PriorityQueueException if the priority is lower then the current
     * priority of the element
     */
    public void increasePriority(QueueElem elem, int indexElem, P priority) throws PriorityQueueException {
        if (this.priorityQueueType == PriorityQueue.MAX_PRIORITY_QUEUE) {
            if (this.comparator.compare(elem.getPriority(), priority) > 0) {
                throw new PriorityQueueException("The new priority is lower then the current priority");
            } else {
                this.heap.heap.get(indexElem).setPriority(priority);
                while (indexElem > 0 && indexElem < this.heap.heap.size() && (this.comparator.compare(this.heap.heap.get(this.heap.getParent(indexElem)).getPriority(),
                        elem.getPriority()) < 0)) {
                    this.heap.swap(indexElem, this.heap.getParent(indexElem));
                    indexElem = this.heap.getParent(indexElem);
                }
            }
        } else {
            throw new PriorityQueueException("Cannot increase priority in a min priority queue");
        }
    }

    /**
     * Decrease the priority of the element
     *
     * @param elem: the elem whose priority is decreased
     * @param indexElem: the index at which the element is stored in the heap
     * @param priority: the new priority of the element
     * @throws PriorityQueueException if the priority is higher then the current
     * priority of the element
     */
    public void decreasePriority(QueueElem elem, int indexElem, P priority) throws PriorityQueueException {
        if (this.priorityQueueType == PriorityQueue.MIN_PRIORITY_QUEUE) {
            if (this.comparator.compare(elem.getPriority(), priority) < 0) {
                throw new PriorityQueueException("The new priority is higher then the current priority");
            } else {
                this.heap.heap.get(indexElem).setPriority(priority);
                while (indexElem > 0 && indexElem < this.heap.heap.size() && (this.comparator.compare(this.heap.heap.get(this.heap.getParent(indexElem)).getPriority(),
                        elem.getPriority()) > 0)) {
                    this.heap.swap(indexElem, this.heap.getParent(indexElem));
                    indexElem = this.heap.getParent(indexElem);
                }
            }
        } else {
            throw new PriorityQueueException("Cannot decrease priority in a max priority queue");
        }
    }

    /**
     * Insert an element in the priority queue
     *
     * @param elem: the element to insert
     * @throws PriorityQueueException if the priority is lower then the current
     * priority of the elements
     */
    public void insert(QueueElem elem) throws PriorityQueueException {
        if (elem != null && this.priorityQueueType == PriorityQueue.MAX_PRIORITY_QUEUE) {
            this.heap.heap.add(elem);
            elem.setQueueElemIndex(this.heap.heap.size() - 1);
            increasePriority(elem, elem.getQueueElemIndex(), (P) elem.getPriority());
        } else if (elem != null && this.priorityQueueType == PriorityQueue.MIN_PRIORITY_QUEUE) {
            this.heap.heap.add(elem);
            elem.setQueueElemIndex(this.heap.heap.size() - 1);
            decreasePriority(elem, elem.getQueueElemIndex(), (P) elem.getPriority());
        } else {
            throw new PriorityQueueException("elem is null");
        }
    }
}
