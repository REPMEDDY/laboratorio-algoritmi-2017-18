package esercizio3_17_18;


import java.util.Comparator;

/**
 *
 * @author soryt
 * @param <P>: the type of the priority of the elements
 */
public class PriorityQueueComparator<P extends Comparable<P>> implements Comparator {

    public PriorityQueueComparator() {
    }

    @Override
    public int compare(Object o1, Object o2) {
        Double d1 = (Double) o1;
        Double d2 = (Double) o2;
        
        if(d1 < d2)
            return -1;
        else if (d1 > d2)
            return 1;
        else
            return 0;
    }
    
}
