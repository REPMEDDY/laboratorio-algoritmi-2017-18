
import esercizio3_17_18.PriorityQueueException;
import esercizio3_17_18.PriorityQueueComparator;
import esercizio3_17_18.PriorityQueue;
import esercizio3_17_18.QueueElem;
import java.util.Comparator;
import java.util.List;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author soryt
 */
public class PriorityQueueTestings {

    private Comparator comparator;
    private PriorityQueue priorityQueueMin;
    private PriorityQueue priorityQueueMax;
    
    private void isMaxQueue(){
        for (int i = 0; i < this.priorityQueueMax.getNumberOfElements() / 2 - 1; i++) {
            QueueElem testEl1 = (QueueElem) this.priorityQueueMax.getPriorityQueueElements().get(i);
            QueueElem testEl2 = (QueueElem) this.priorityQueueMax.getPriorityQueueElements().get((2 * i) + 1);
            QueueElem testEl3 = (QueueElem) this.priorityQueueMax.getPriorityQueueElements().get((2 * i) + 2);
            assertTrue((this.comparator.compare(testEl1.getPriority(), testEl2.getPriority()) >= 0)
                    == (this.comparator.compare(testEl1.getPriority(), testEl3.getPriority()) >= 0));
        }
    }
    
    private void isMinQueue(){
        for (int i = 0; i < this.priorityQueueMin.getNumberOfElements() / 2 - 1; i++) {
            QueueElem testEl1 = (QueueElem) this.priorityQueueMin.getPriorityQueueElements().get(i);
            QueueElem testEl2 = (QueueElem) this.priorityQueueMin.getPriorityQueueElements().get((2 * i) + 1);
            QueueElem testEl3 = (QueueElem) this.priorityQueueMin.getPriorityQueueElements().get((2 * i) + 2);
            assertTrue((this.comparator.compare(testEl1.getPriority(), testEl2.getPriority()) <= 0)
                    == (this.comparator.compare(testEl1.getPriority(), testEl3.getPriority()) <= 0));
        }
    }

    @Before
    public void setUpTests() {
        this.comparator = new PriorityQueueComparator();
        this.priorityQueueMax = new PriorityQueue(this.comparator, PriorityQueue.MAX_PRIORITY_QUEUE);
        this.priorityQueueMin = new PriorityQueue(this.comparator, PriorityQueue.MIN_PRIORITY_QUEUE);
    }
    
    //start test
    @Test
    public void testOnInsertEqualPriorityElements() throws PriorityQueueException {
        QueueElem el1 = new QueueElem(17.0, "aaa");
        QueueElem el2 = new QueueElem(17.0, "cgcx");
        QueueElem el5 = new QueueElem(17.0, 3);
        QueueElem el3 = new QueueElem(18.0, "rgyyt");
        QueueElem el4 = new QueueElem(17.0, "ecerec");
        this.priorityQueueMin.insert(el1);
        this.priorityQueueMin.insert(el2);
        this.priorityQueueMin.insert(el5);
        this.priorityQueueMin.insert(el3);
        this.priorityQueueMin.insert(el4);
        this.isMinQueue();
    }
    //end test
    
    //start test
    @Test
    public void testOnInsertElementMax() throws PriorityQueueException {
        QueueElem el1 = new QueueElem(17.0, "aaa");
        QueueElem el2 = new QueueElem(18.0, "cgcx");
        QueueElem el5 = new QueueElem(100.0, false);
        QueueElem el3 = new QueueElem(19.0, "rgyyt");
        QueueElem el4 = new QueueElem(20.0, "ecerec");
        this.priorityQueueMax.insert(el1);
        this.priorityQueueMax.insert(el2);
        this.priorityQueueMax.insert(el5);
        this.priorityQueueMax.insert(el3);
        this.priorityQueueMax.insert(el4);
        this.isMaxQueue();
    }
    //end test

    //start test
    @Test
    public void testOnInsertElementMin() throws PriorityQueueException {
        QueueElem el1 = new QueueElem(17.0, "aaa");
        QueueElem el2 = new QueueElem(18.0, "cgcx");
        QueueElem el5 = new QueueElem(100.0, false);
        QueueElem el3 = new QueueElem(19.0, "rgyyt");
        QueueElem el4 = new QueueElem(20.0, "ecerec");
        this.priorityQueueMin.insert(el1);
        this.priorityQueueMin.insert(el2);
        this.priorityQueueMin.insert(el5);
        this.priorityQueueMin.insert(el3);
        this.priorityQueueMin.insert(el4);
        this.isMinQueue();
    }
    //end test

    //start test
    @Test
    public void testOnIncreaseKeyMax() throws PriorityQueueException {
        QueueElem el1 = new QueueElem(17.0, "aaa");
        QueueElem el2 = new QueueElem(18.0, "cgcx");
        QueueElem el5 = new QueueElem(100.0, false);
        QueueElem el3 = new QueueElem(19.0, "rgyyt");
        QueueElem el4 = new QueueElem(20.0, "ecerec");
        this.priorityQueueMax.insert(el1);
        this.priorityQueueMax.insert(el2);
        this.priorityQueueMax.insert(el5);
        this.priorityQueueMax.insert(el3);
        this.priorityQueueMax.insert(el4);
        double newPriority = 190.0;
        List<Pair <QueueElem, Integer>> p = (List) this.priorityQueueMax.search(19.0);
        this.priorityQueueMax.increasePriority(el3,p.get(0).getValue() , newPriority);
        assertTrue(this.comparator.compare(newPriority, el3.getPriority()) == 0);
        this.isMaxQueue();
    }
    //end test

    //start test
    @Test
    public void testOnDecreaseKeyMin() throws PriorityQueueException {
        QueueElem el1 = new QueueElem(17.0, "aaa");
        QueueElem el2 = new QueueElem(18.0, "cgcx");
        QueueElem el5 = new QueueElem(100.0, false);
        QueueElem el3 = new QueueElem(19.0, "rgyyt");
        QueueElem el4 = new QueueElem(20.0, "ecerec");
        this.priorityQueueMin.insert(el1);
        this.priorityQueueMin.insert(el2);
        this.priorityQueueMin.insert(el5);
        this.priorityQueueMin.insert(el3);
        this.priorityQueueMin.insert(el4);
        double newPriority = 10.0;
        List<Pair <QueueElem, Integer>> p = (List) this.priorityQueueMin.search(20.0);
        this.priorityQueueMin.decreasePriority(el4, p.get(0).getValue(), newPriority);
        assertTrue(this.comparator.compare(newPriority, el4.getPriority()) == 0);
        this.isMinQueue();
    }
    //end test

    //start test
    @Test
    public void testOnExtractMaximum() throws PriorityQueueException {
        QueueElem el1 = new QueueElem(17.0, "aaa");
        QueueElem el2 = new QueueElem(18.0, "cgcx");
        QueueElem el5 = new QueueElem(100.0, false);
        QueueElem el3 = new QueueElem(19.0, "rgyyt");
        QueueElem el4 = new QueueElem(20.0, "ecerec");
        this.priorityQueueMax.insert(el1);
        this.priorityQueueMax.insert(el2);
        this.priorityQueueMax.insert(el5);
        this.priorityQueueMax.insert(el3);
        this.priorityQueueMax.insert(el4);
        int sizeBefore = this.priorityQueueMax.getNumberOfElements();
        assertTrue(el5.getPriority() == this.priorityQueueMax.extractMax().getPriority()
                && this.priorityQueueMax.getNumberOfElements() == sizeBefore - 1);
        this.isMaxQueue();
    }
    //end test

    //start test
    @Test
    public void testOnExtractMinimum() throws PriorityQueueException {
        QueueElem el2 = new QueueElem(18.0, "cgcx");
        QueueElem el5 = new QueueElem(100.0, false);
        QueueElem el3 = new QueueElem(19.0, "rgyyt");
        QueueElem el4 = new QueueElem(20.0, "ecerec");
        QueueElem el1 = new QueueElem(17.0, "aaa");
        this.priorityQueueMin.insert(el1);
        this.priorityQueueMin.insert(el2);
        this.priorityQueueMin.insert(el5);
        this.priorityQueueMin.insert(el3);
        this.priorityQueueMin.insert(el4);
        int sizeBefore = this.priorityQueueMin.getNumberOfElements();
        assertTrue(el1.getPriority() == this.priorityQueueMin.extractMin().getPriority()
                && this.priorityQueueMin.getNumberOfElements() == sizeBefore - 1);
        this.isMinQueue();
    }
    //end test

    //start test
    @Test
    public void testOnGetMaximum() throws PriorityQueueException {
        QueueElem el1 = new QueueElem(22.0, "aaa");
        QueueElem el2 = new QueueElem(18.0, "cgcx");
        QueueElem el5 = new QueueElem(100.0, false);
        QueueElem el3 = new QueueElem(19.0, "rgyyt");
        QueueElem el4 = new QueueElem(20.0, "ecerec");
        this.priorityQueueMax.insert(el1);
        this.priorityQueueMax.insert(el2);
        this.priorityQueueMax.insert(el5);
        this.priorityQueueMax.insert(el3);
        this.priorityQueueMax.insert(el4);
        assertTrue(this.comparator.compare(el5.getPriority(), this.priorityQueueMax.maximum().getPriority()) == 0);
    }
    //end test

    //start test
    @Test
    public void testOnGetMinimum() throws PriorityQueueException {
        QueueElem el2 = new QueueElem(18.0, "cgcx");
        QueueElem el5 = new QueueElem(100.0, false);
        QueueElem el3 = new QueueElem(19.0, "rgyyt");
        QueueElem el4 = new QueueElem(20.0, "ecerec");
        QueueElem el1 = new QueueElem(17.0, "aaa");
        this.priorityQueueMin.insert(el1);
        this.priorityQueueMin.insert(el2);
        this.priorityQueueMin.insert(el5);
        this.priorityQueueMin.insert(el3);
        this.priorityQueueMin.insert(el4);
        assertTrue(this.comparator.compare(el1.getPriority(), this.priorityQueueMin.minimum().getPriority()) == 0);
    }
    //end test
}
