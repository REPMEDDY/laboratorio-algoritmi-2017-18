# Relazione esercizio 1 Lab ASD 17/18

L'esercizio 1 prevede lo sviluppo di una libreria che fornisca gli algoritmi di ordinamento seguenti:

- Insertion Sort
- Merge Sort

Tali algoritmi sono stati messi alla prova ordinando 20 milioni di elementi

## Risultati attesi per l'Insertion Sort
Il risultato atteso provando ad ordinare N elementi con l'Insertion Sort è che all'aumentare di N, i tempi di ordinamento aumentano molto in fretta.
Questo succede perchè l'algoritmo per individuare la posizione corretta in cui posizionare l'elemento n-esimo da ordinare deve in generale effettuare n-1 confronti. La complessità
risulta quindi quadratica.

## Risultati ottenuti per il Merge Sort
L'algoritmo è stato lasciato in esecuzione per più di 10 minuti oltre i quali l'esecuzione è stata interrotta. Come previsto, su input di taglia notevole come 20 milioni di elementi
l'algoritmo impiega troppo tempo per ordinare gli elementi, rendendo l'Insertion Sort utilizzabile solo su input di taglia piccola.


## Risultati attesi per il Merge Sort
Il risultato atteso provando ad ordinare N elementi con il Merge Sort è che all'aumentare di N, i tempi di ordinamento aumentano lentamente.
Questo succede perchè il Merge Sort implementa il criterio del Divide et Impera, ovvero scompone il problema di ordinare N elementi i sottoproblemi della stessa forma ma risolvibili
più semplicemente sfruttando la ricorsione. Infine combina i risultati ottenuti dai sottoproblemi per risolvere il problema principale.
In particolare il Merge Sort dovendo ordinare N elementi scompone il problema ordinando i primi N/2 elementi e poi i secondi N/2 e cosi via ricorsivamente. Di conseguenza per 
determinare la posizione corretta in cui posizionare l'elemento n-esimo da ordinare, il Merge Sort in generale effettua log(n) confronti. La complessità risulta quindi logaritmica.

## Risultati ottenuti per il Merge Sort
L'algoritmo ha ordinato i 20 milioni di elementi in 65 secondi. Come previsto all'aumentare del numero di elementi da ordinare, i tempi di ordinamento aumentano lentamente rendendo
il Merge Sort utilizzabile sia su input di piccola taglia sia su input di grossa taglia.

## Implementazione di un test su un array di interi
L'esercizio 1 prevede infine l'implementazione di una funzione che accetta in input un intero N e un qualunque array A di interi e che verifica se A contiene due interi
la cui somma è esattamente N. Si richiede che la funzione abbia complessità $\Theta(KlogK)$ sul numero K di elementi dell'array A.

### Elenco degli elementi N e degli elementi A e B tali che N = A + B.  / = non trovato

| N 	                        | A         	| B         	|
|---------------------------	|-------------	|--------------	|
| 178045131                  	|     176357141 |     1687990  	|
| 154266748                 	|     153827684 |     439064   |
| 93573006             	        |     88725992  |     4847014   |
| 191011796                     |     190553416 |     458380   |
| 63744743                      |     60686298  |     3058445  |
| 139183879                     |     139179682 |     4197     |
| 102116441                     |     100983030 |     1133411  |
| 166015103                     |     165681155 |     333948   |
| 100000000                     |     99634049  |     365951   |
| 100                           |     /         |      /        |
| 40000000000                   |     /         |      /        |


Per far si che tale funzione abbia la complessità richiesta è stato utilizzato il Merge Sort per ordinare l'array A in tempo nlog(n) e per trovare gli elementi A e B è stata 
utilizzata la ricerca dicotomica che impiega tempo log(n) per trovare un elemento in un array. Cosi facendo si ottiene nlog(n) + log(n) = Theta(nlog(n)).
Gli elementi A e B vengono trovati in tempo zero.

# Relazione esercizio 2 Lab ASD 17/18
L'esercizio 2 prevede l'implementazione di una funzione che calcola l'edit distance tra due stringhe s1 e s2.

In particolare si richiedono due versioni:
- versione ricorsiva
- versione che segue i principi della programmazione dinamica

## Versione ricorsiva
La versione ricorsiva è stata realizzata implementando direttamente la definizione ricorsiva fornita nel testo dell'esercizio 2.

## Versione che segue i principi della programmazine dinamica
La versione che segue i principi della programmazione dinamica è stata realizzata calcolando la più lunga sottosequenza comune(LCS) tra le due stringhe s1 e s2. Conoscendo la LCS
delle due stringhe s1 e s2 si conosce il numero di caratteri comuni a s1 e s2 che quindi non vengono considerati nel calcolo dell'edit distance.

Si chiede poi di realizzare una applicazione che utilizzando la versione dinamica della funzione edit distance calcoli per ogni parola W in correctme.txt,
la lista di parole in dictionary.txt con edit distance minima da W.

### Mandando in esecuzione tale applicazione i risultati ottenuti sono i seguenti:
#### Min Edit_distance for string "Quando": 2
    -List of min Edit_distance strings from string "Quando": 
    -String "ando" -> Edit_distance: 2
    -String "quando" -> Edit_distance: 2
    -String "usando" -> Edit_distance: 2


#### Min Edit_distance for string "avevo": 0
    -List of min Edit_distance strings from string "avevo": 
    -String "avevo" -> Edit_distance: 0



#### Min Edit_distance for string "cinqve": 2
    -List of min Edit_distance strings from string "cinqve": 
    -String "cinque" -> Edit_distance: 2
    -String "cive" -> Edit_distance: 2



#### Min Edit_distance for string "anni": 0
    -List of min Edit_distance strings from string "anni": 
    -String "anni" -> Edit_distance: 0



#### Min Edit_distance for string "mia": 0
    -List of min Edit_distance strings from string "mia": 
    -String "mia" -> Edit_distance: 0



#### Min Edit_distance for string "made": 0
    -List of min Edit_distance strings from string "made": 
    -String "made" -> Edit_distance: 0



#### Min Edit_distance for string "mi": 0
    -List of min Edit_distance strings from string "mi": 
    -String "mi" -> Edit_distance: 0



#### Min Edit_distance for string "perpeteva": 3
    -List of min Edit_distance strings from string "perpeteva": 
    -String "erpete" -> Edit_distance: 3
    -String "permetteva" -> Edit_distance: 3
    -String "perpendeva" -> Edit_distance: 3
    -String "perpetra" -> Edit_distance: 3
    -String "perpetrava" -> Edit_distance: 3
    -String "perpetrera" -> Edit_distance: 3
    -String "perpetua" -> Edit_distance: 3
    -String "perpetuava" -> Edit_distance: 3
    -String "perpetue" -> Edit_distance: 3
    -String "perpetuera" -> Edit_distance: 3
    -String "repeteva" -> Edit_distance: 3
    -String "ripeteva" -> Edit_distance: 3



#### Min Edit_distance for string "sempre": 0
    -List of min Edit_distance strings from string "sempre": 
    -String "sempre" -> Edit_distance: 0



#### Min Edit_distance for string "che": 0
    -List of min Edit_distance strings from string "che": 
    -String "che" -> Edit_distance: 0



#### Min Edit_distance for string "la": 0
    -List of min Edit_distance strings from string "la": 
    -String "la" -> Edit_distance: 0



#### Min Edit_distance for string "felicita": 0
    -List of min Edit_distance strings from string "felicita": 
    -String "felicita" -> Edit_distance: 0



#### Min Edit_distance for string "e": 0
    -List of min Edit_distance strings from string "e": 
    -String "e" -> Edit_distance: 0



#### Min Edit_distance for string "la": 0
    -List of min Edit_distance strings from string "la": 
    -String "la" -> Edit_distance: 0



#### Min Edit_distance for string "chiave": 0
    -List of min Edit_distance strings from string "chiave": 
    -String "chiave" -> Edit_distance: 0



#### Min Edit_distance for string "della": 0
    -List of min Edit_distance strings from string "della": 
    -String "della" -> Edit_distance: 0



#### Min Edit_distance for string "vita": 0
    -List of min Edit_distance strings from string "vita": 
    -String "vita" -> Edit_distance: 0



#### Min Edit_distance for string "Quando": 2
    -List of min Edit_distance strings from string "Quando": 
    -String "ando" -> Edit_distance: 2
    -String "quando" -> Edit_distance: 2
    -String "usando" -> Edit_distance: 2



#### Min Edit_distance for string "andai": 0
    -List of min Edit_distance strings from string "andai": 
    -String "andai" -> Edit_distance: 0



#### Min Edit_distance for string "a": 0
    -List of min Edit_distance strings from string "a": 
    -String "a" -> Edit_distance: 0



#### Min Edit_distance for string "squola": 1
    -List of min Edit_distance strings from string "squola": 
    -String "suola" -> Edit_distance: 1



#### Min Edit_distance for string "mi": 0
    -List of min Edit_distance strings from string "mi": 
    -String "mi" -> Edit_distance: 0



#### Min Edit_distance for string "domandrono": 1
    -List of min Edit_distance strings from string "domandrono": 
    -String "domandarono" -> Edit_distance: 1



#### Min Edit_distance for string "come": 0
    -List of min Edit_distance strings from string "come": 
    -String "come" -> Edit_distance: 0



#### Min Edit_distance for string "vuolessi": 1
    -List of min Edit_distance strings from string "vuolessi": 
    -String "volessi" -> Edit_distance: 1



#### Min Edit_distance for string "essere": 0
    -List of min Edit_distance strings from string "essere": 
    -String "essere" -> Edit_distance: 0



#### Min Edit_distance for string "da": 0
    -List of min Edit_distance strings from string "da": 
    -String "da" -> Edit_distance: 0



#### Min Edit_distance for string "grande": 0
    -List of min Edit_distance strings from string "grande": 
    -String "grande" -> Edit_distance: 0



#### Min Edit_distance for string "Io": 2
    -List of min Edit_distance strings from string "Io": 
    -String "bo" -> Edit_distance: 2
    -String "co" -> Edit_distance: 2
    -String "do" -> Edit_distance: 2
    -String "eo" -> Edit_distance: 2
    -String "fo" -> Edit_distance: 2
    -String "go" -> Edit_distance: 2
    -String "ho" -> Edit_distance: 2
    -String "io" -> Edit_distance: 2
    -String "ko" -> Edit_distance: 2
    -String "lo" -> Edit_distance: 2
    -String "mo" -> Edit_distance: 2
    -String "no" -> Edit_distance: 2
    -String "oc" -> Edit_distance: 2
    -String "od" -> Edit_distance: 2
    -String "oe" -> Edit_distance: 2
    -String "oh" -> Edit_distance: 2
    -String "oi" -> Edit_distance: 2
    -String "ok" -> Edit_distance: 2
    -String "on" -> Edit_distance: 2
    -String "or" -> Edit_distance: 2
    -String "po" -> Edit_distance: 2
    -String "ro" -> Edit_distance: 2
    -String "so" -> Edit_distance: 2
    -String "to" -> Edit_distance: 2
    -String "vo" -> Edit_distance: 2



#### Min Edit_distance for string "scrissi": 0
    -List of min Edit_distance strings from string "scrissi": 
    -String "scrissi" -> Edit_distance: 0



#### Min Edit_distance for string "selice": 0
    -List of min Edit_distance strings from string "selice": 
    -String "selice" -> Edit_distance: 0



#### Min Edit_distance for string "Mi": 2
    -List of min Edit_distance strings from string "Mi": 
    -String "ai" -> Edit_distance: 2
    -String "bi" -> Edit_distance: 2
    -String "ci" -> Edit_distance: 2
    -String "di" -> Edit_distance: 2
    -String "ei" -> Edit_distance: 2
    -String "fi" -> Edit_distance: 2
    -String "gi" -> Edit_distance: 2
    -String "hi" -> Edit_distance: 2
    -String "id" -> Edit_distance: 2
    -String "ih" -> Edit_distance: 2
    -String "il" -> Edit_distance: 2
    -String "in" -> Edit_distance: 2
    -String "io" -> Edit_distance: 2
    -String "li" -> Edit_distance: 2
    -String "mi" -> Edit_distance: 2
    -String "ni" -> Edit_distance: 2
    -String "oi" -> Edit_distance: 2
    -String "pi" -> Edit_distance: 2
    -String "si" -> Edit_distance: 2
    -String "ti" -> Edit_distance: 2
    -String "vi" -> Edit_distance: 2
    -String "xi" -> Edit_distance: 2
    -String "zi" -> Edit_distance: 2



#### Min Edit_distance for string "dissero": 0
    -List of min Edit_distance strings from string "dissero": 
    -String "dissero" -> Edit_distance: 0



#### Min Edit_distance for string "che": 0
    -List of min Edit_distance strings from string "che": 
    -String "che" -> Edit_distance: 0



#### Min Edit_distance for string "non": 0
    -List of min Edit_distance strings from string "non": 
    -String "non" -> Edit_distance: 0



#### Min Edit_distance for string "avevo": 0
    -List of min Edit_distance strings from string "avevo": 
    -String "avevo" -> Edit_distance: 0



#### Min Edit_distance for string "capito": 0
    -List of min Edit_distance strings from string "capito": 
    -String "capito" -> Edit_distance: 0



#### Min Edit_distance for string "il": 0
    -List of min Edit_distance strings from string "il": 
    -String "il" -> Edit_distance: 0



#### Min Edit_distance for string "corpito": 2
    -List of min Edit_distance strings from string "corpito": 
    -String "carpito" -> Edit_distance: 2
    -String "clorito" -> Edit_distance: 2
    -String "coito" -> Edit_distance: 2
    -String "colpito" -> Edit_distance: 2
    -String "compito" -> Edit_distance: 2
    -String "copiato" -> Edit_distance: 2
    -String "copio" -> Edit_distance: 2
    -String "copto" -> Edit_distance: 2
    -String "corio" -> Edit_distance: 2
    -String "corpi" -> Edit_distance: 2
    -String "corpino" -> Edit_distance: 2
    -String "corpo" -> Edit_distance: 2
    -String "corto" -> Edit_distance: 2
    -String "covrito" -> Edit_distance: 2
    -String "crepito" -> Edit_distance: 2
    -String "scorpio" -> Edit_distance: 2



#### Min Edit_distance for string "e": 0
    -List of min Edit_distance strings from string "e": 
    -String "e" -> Edit_distance: 0



#### Min Edit_distance for string "io": 0
    -List of min Edit_distance strings from string "io": 
    -String "io" -> Edit_distance: 0



#### Min Edit_distance for string "dissi": 0
    -List of min Edit_distance strings from string "dissi": 
    -String "dissi" -> Edit_distance: 0



#### Min Edit_distance for string "loro": 0
    -List of min Edit_distance strings from string "loro": 
    -String "loro" -> Edit_distance: 0



#### Min Edit_distance for string "che": 0
    -List of min Edit_distance strings from string "che": 
    -String "che" -> Edit_distance: 0



#### Min Edit_distance for string "non": 0
    -List of min Edit_distance strings from string "non": 
    -String "non" -> Edit_distance: 0



#### Min Edit_distance for string "avevano": 0
    -List of min Edit_distance strings from string "avevano": 
    -String "avevano" -> Edit_distance: 0



#### Min Edit_distance for string "capito": 0
    -List of min Edit_distance strings from string "capito": 
    -String "capito" -> Edit_distance: 0



#### Min Edit_distance for string "la": 0
    -List of min Edit_distance strings from string "la": 
    -String "la" -> Edit_distance: 0



#### Min Edit_distance for string "wita": 2
    -List of min Edit_distance strings from string "wita": 
    -String "aita" -> Edit_distance: 2
    -String "cita" -> Edit_distance: 2
    -String "dita" -> Edit_distance: 2
    -String "gita" -> Edit_distance: 2
    -String "iota" -> Edit_distance: 2
    -String "irta" -> Edit_distance: 2
    -String "ista" -> Edit_distance: 2
    -String "iuta" -> Edit_distance: 2
    -String "lita" -> Edit_distance: 2
    -String "pita" -> Edit_distance: 2
    -String "sita" -> Edit_distance: 2
    -String "ta" -> Edit_distance: 2
    -String "vita" -> Edit_distance: 2
    -String "witz" -> Edit_distance: 2
    -String "zita" -> Edit_distance: 2

